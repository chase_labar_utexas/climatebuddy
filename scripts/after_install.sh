sudo cp -r /home/ubuntu/my_app/* /var/www/html

cd /home/ubuntu/my_app


#
# backend docker deployment
#

sudo docker build -t climate_buddy.backend -f backend/backend_dockerfile .

sudo docker stop cb.backend
sudo docker rm cb.backend

sudo docker run -d -e POSTGRES_USER=postgres -e POSTGRES_HOST=pg-docker -e POSTGRES_DB=postgres -e POSTGRES_PORT=5432 -e POSTGRES_PASSWORD=docker -e FLASK_RUN_PORT=8080 -e FLASK_RUN_HOST=0.0.0.0 --name cb.backend --link pg-docker:pg-docker -p 8080:80 climate_buddy.backend

sudo docker system prune -f

#
# frontend docker deployment
#

sudo docker stop cb.frontend
sudo docker rm cb.frontend

sudo docker system prune -f

sudo docker build -t climate_buddy.frontend -f frontend/frontend_dockerfile .

sudo docker run -d -i --name cb.frontend -p 80:3000 climate_buddy.frontend

sudo docker system prune -f

at -M now + 2 minute <<< $'service codedeploy-agent restart'