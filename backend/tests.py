# -*- coding: utf-8 -*-
import unittest

from flask_app import city_row_dict, country_row_dict, wrap_header
from scrape import remove_accents, scrape_country_info

# creates DummyObj for use in testing functions expecting an object with specific properties
class DummyObj:
    val = 5

# creates DummyDbSession for use in testing functions expecting an object with specific properties and functions
class DummyDbSession:
    def merge(self, i):
        pass

    def commit(self):
        pass

class TestBackend(unittest.TestCase):
    # test method flask_app.country_row_dict(data, info)
    def country_row_dict_test(self):
        info = DummyObj()
        info.id = 1
        info.country_name = 1
        data = DummyObj()
        data.GDP_per_capita = 1
        data.CO2_per_capita = 1
        data.total_GDP = 1
        data.signed_paris_agreement = 1
        data.average_lifespan = 1
        data.population = 1
        data.continent = 1
        data.hdi = 1
        data.CO2_div_GDP = 1
        data.GGE = 1
        data.percent_renewable = 1
        result = country_row_dict(data, info)
        for k in result.keys():
            self.assertEqual(result[k], 1)

    # test method flask_app.city_row_dict(data, info)
    def city_row_dict_test(self):
        info = DummyObj()
        info.id = 1
        info.city_name = 1
        data = DummyObj()
        data.aqi = 1
        data.population = 1
        info.country_id = 1
        data.is_threatened = 1
        data.elevation = 1
        data.GGE = 1
        data.pm25 = 1
        data.CO2_per_capita = 1
        data.un_development_group = 1
        data.total_builtup_area = 1
        data.climate_description = 1
        data.precipitation_2014_mm = 1
        data.percent_greenness = 1
        result = city_row_dict(data, info)
        for k in result.keys():
            self.assertEqual(result[k], 1)
    
    # test method flask_app.wrap_header(json_resp)
    def test_wrap_header(self):
        o = DummyObj()
        o.headers = {}
        o = wrap_header(o)
        self.assertEqual(o.headers["Access-Control-Allow-Origin"], "*")

    # test method scrape.wrap_header(json_resp)
    def test_remove_accents(self):
        test_str = "Torreón Mexico"
        result = remove_accents(test_str)
        self.assertEqual(result, "Torreon Mexico")

    # test method scrape.scrape_country_info(db)
    def test_scrape_country_info(self):
        db = DummyObj()
        db.session = DummyDbSession()
        result = scrape_country_info(db)
        self.assertEqual(True, len(result) > 0)


if __name__ == "__main__":
    unittest.main(verbosity=2)
