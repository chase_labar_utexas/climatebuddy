import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()


class CountryInfo(db.Model):
    __tablename__ = "country_info"
    id = db.Column(db.Integer, primary_key=True)
    country_name = db.Column(db.String(100))
    country_iso = db.Column(db.String(3))
    country_two = db.Column(db.String(2))


class CountryData(db.Model):
    __tablename__ = "country_data"
    id = db.Column(db.Integer, primary_key=True)
    GDP_per_capita = db.Column(db.Float)
    CO2_per_capita = db.Column(db.Float)
    CO2_div_GDP = db.Column(db.Float)
    total_GDP = db.Column(db.Float)
    signed_paris_agreement = db.Column(db.Boolean)
    GGE = db.Column(db.Float)
    average_lifespan = db.Column(db.Float)
    population = db.Column(db.Integer)
    continent = db.Column(db.String(100))
    hdi = db.Column(db.Float)
    percent_renewable = db.Column(db.Float)


class CityInfo(db.Model):
    __tablename__ = "city_info"
    id = db.Column(db.Integer, primary_key=True)
    city_name = db.Column(db.String(100))
    country_id = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)


class CityData(db.Model):
    __tablename_ = "city_data"
    id = db.Column(db.Integer, primary_key=True)
    country_id = db.Column(db.Integer)
    population = db.Column(db.Integer)
    elevation = db.Column(db.Float)
    aqi = db.Column(db.Float)
    pm25 = db.Column(db.Float)
    GGE = db.Column(db.Float)
    CO2_per_capita = db.Column(db.Float)
    is_threatened = db.Column(db.Boolean)
    un_development_group = db.Column(db.String(5))
    total_builtup_area = db.Column(db.Float)
    climate_description = db.Column(db.String(100))
    precipitation_2014_mm = db.Column(db.Float)
    percent_greenness = db.Column(db.Float)


class StatInfo(db.Model):
    __tablename__ = "stat_info"
    stat_id = db.Column(db.Integer, primary_key=True)
    stat_table = db.Column(db.Float)
    stat_table_col = db.Column(db.String)


class StatData(db.Model):
    __tablename__ = "stat_data"
    stat_id = db.Column(db.Integer, primary_key=True)
    high_id = db.Column(db.Integer)
    high_val = db.Column(db.Float)
    low_id = db.Column(db.Integer)
    low_val = db.Column(db.Float)
    mean = db.Column(db.Float)
    median = db.Column(db.Float)
    first_quartile = db.Column(db.Float)
    third_quartile = db.Column(db.Float)
    std_dev = db.Column(db.Float)
    num_samples = db.Column(db.Integer)
    pop = db.Column(db.Integer)
