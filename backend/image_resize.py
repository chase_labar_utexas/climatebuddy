import boto3, botocore
from boto3 import client, s3
import requests, json, time
import PIL
from PIL import Image

BUCKET_NAME = "climate.buddy.data"
k = 'AKIARI6AU3GFW27DIM4X'
sec = '4XVb/0H4B6mQ4sUqEYuovUyHQq09ftgD5Kn4B3hB'
s3 = boto3.resource('s3', aws_access_key_id=k, aws_secret_access_key=sec, region_name='us-east-2')
s3c = client('s3', aws_access_key_id=k, aws_secret_access_key=sec, region_name='us-east-2')

response = requests.get(
                "https://api.climatebuddy.xyz/cities").json()
for city in response:
    try:
        city_id = city['cityID']
        obj_name = "city_photos/" + str(city_id) + ".jpg"
        local_filename = "temp_city_photo.jpg"
        with open(local_filename, 'wb') as f:
            s3c.download_fileobj(BUCKET_NAME, obj_name, f)

        time.sleep(0.01)

        basemax = 140
        img = Image.open(local_filename)
        if img.size[0] > img.size[1]:
            # width is bigger
            wpercent = (basemax / float(img.size[0]))
            hsize = int((float(img.size[1]) * float(wpercent)))
            img = img.resize((basemax, hsize), PIL.Image.ANTIALIAS)
        else:
            # height is bigger
            hpercent = (basemax / float(img.size[1]))
            wsize = int((float(img.size[0]) * float(hpercent)))
            img = img.resize((wsize, basemax), PIL.Image.ANTIALIAS)
        img.save(local_filename)

        time.sleep(0.01)

        up_resp = s3c.upload_file(local_filename, BUCKET_NAME, "city_icons_" + str(basemax) + "/" + str(city_id) + ".jpg",
                                  ExtraArgs={'ServerSideEncryption': "AES256", 'ACL': 'public-read'})
        print("success " + str(city_id))

    except Exception:
        print("failed " + str(city_id))