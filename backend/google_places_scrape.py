import googlemaps
from googlemaps import places
import boto3, botocore
from boto3 import client, s3
import requests, json, time

k = 'AKIARI6AU3GFW27DIM4X'
sec = '4XVb/0H4B6mQ4sUqEYuovUyHQq09ftgD5Kn4B3hB'
s3 = boto3.resource('s3', aws_access_key_id=k, aws_secret_access_key=sec, region_name='us-east-2')
s3c = client('s3', aws_access_key_id=k, aws_secret_access_key=sec, region_name='us-east-2')

BUCKET_NAME = "climate.buddy.data"

key = "AIzaSyDskNCPV77Ep84fYttQLSypWUnOi-WZJW0"
gmaps = googlemaps.Client(key=key)
# response = places.find_place(gmaps,"cities", 'textquery',fields=["photos"],location_bias='point:36,-115')
response = requests.get(
                "https://api.climatebuddy.xyz/cities").json()
for city in response:
    try:
        lat = city['lat']
        lon = city['lon']
        loc = (lat, lon)
        city_name = city['name']
        city_id = city['cityID']
        response = places.places(gmaps, city_name, loc, type='(cities)')
        photo_id = response['results'][0]['photos'][0]['photo_reference']
        photo = places.places_photo(gmaps, photo_id, max_width=600, max_height=600)
        local_filename = "temp_city_photo.jpg"
        f = open(local_filename, 'wb')
        for chunk in photo:
            if chunk:
                f.write(chunk)
        f.close()

        up_resp = s3c.upload_file(local_filename, BUCKET_NAME, "city_photos/" + str(city_id) + ".jpg",
                                  ExtraArgs={'ServerSideEncryption': "AES256", 'ACL': 'public-read'})
        print("upload " + str(city))
    except Exception:
        print("failed " + str(city))

time.sleep(10)