import json, requests, csv, itertools, unidecode, time, re, statistics

from models import CountryInfo, CountryData, CityInfo, CityData, StatInfo, StatData

country_iso_to_id = {}


def scrape_main(db):
    # global country_iso_to_id
    # country_iso_to_id = scrape_country_info(db)
    # scrape_country_pop(db)
    # scrape_city_info(db)
    # scrape_carbon_footprint_csv(db)
    # scrape_urban_dev_csv(db)
    # scrape_city_data(db)
    # scrape_stat_info(db)
    # scrape_stat_data(db)
    scrape_iso2(db)
    return "GOOD"

# filling country_info table using worldbank api data
def scrape_country_info(db):
    country_list = []
    for x in range(1, 6 + 1):
        response = requests.get(
            "http://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?format=json&date=2018&page="
            + str(x)
        )
        todos = json.loads(response.text)[1]
        if x == 1:
            todos = todos[47:]
        for c in todos:
            country_list.append(c)
            # print(c, flush=True)

    data_list = []
    for c in country_list:
        item = {}
        # print(c, flush=True)
        # print(c.keys())
        item["country_iso"] = c["countryiso3code"]
        item["country_name"] = c["country"]["value"]
        item["population"] = c["value"]
        if not (None in item.values()):
            data_list.append(item)

    iso_id_dict = {}
    for id, c in enumerate(data_list):
        country_info = CountryInfo(
            id=id, country_name=c["country_name"], country_iso=c["country_iso"]
        )
        db.session.merge(country_info)
        db.session.commit()
        iso_id_dict[c["country_iso"]] = id

    print("added " + str(len(data_list)) + " countries to db")
    return iso_id_dict

# filling city_info table using JohnSnowLabs's 'country-and-continent-codes-list' api data
def scrape_city_info(db):
    with open("pop.csv", newline="", encoding="utf-8") as csvfile:
        print(csvfile, flush=True)
        res = json.loads(
            requests.get(
                "https://pkgstore.datahub.io/JohnSnowLabs/country-and-continent-codes-list/country-and-continent-codes-list-csv_json/data/c218eebbf2f8545f3db9051ac893d69c/country-and-continent-codes-list-csv_json.json"
            ).text
        )

        # print(res)
        country_ison_to_iso3 = {}
        for country in res:
            country_ison_to_iso3[country["Country_Number"]] = country[
                "Three_Letter_Country_Code"
            ]
        reader = csv.reader(csvfile)
        next(reader)
        i = 0
        for row in itertools.islice(reader, 500):
            id = i
            city_name = unidecode.unidecode(row[3])
            if "(" in city_name:
                p = re.compile("\((.+)\)")
                city_name = p.search(city_name).group(1)
            iso3 = country_ison_to_iso3.get(int(row[0]))
            country_id = country_iso_to_id.get(iso3)
            lat = row[5]
            lon = row[6]
            population = int(row[21].replace(",", "")) * 1000
            city_info = CityInfo(
                id=id, country_id=country_id, city_name=city_name, lat=lat, lon=lon
            )
            city_data = CityData(id=id, population=population)
            i = i + 1
            db.session.merge(city_info)
            db.session.merge(city_data)
            db.session.commit()

    return "hello"

# filling country_data table using worldbank api data
def scrape_country_pop(db):
    country_list = []
    for x in range(1, 6 + 1):
        response = requests.get(
            "http://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?format=json&date=2018&page="
            + str(x)
        )
        todos = json.loads(response.text)[1]
        if x == 1:
            todos = todos[47:]
        for c in todos:
            country_list.append(c)
            print(c, flush=True)

    global country_iso_to_id
    data_list = []
    for c in country_list:
        item = {}
        print(c, flush=True)
        print(c.keys())
        item["population"] = c["value"]
        item["country_iso"] = c["countryiso3code"]
        if not (None in item.values()):
            item["population"] = int(item["population"])
            data_list.append(item)

    country_iso_to_co2_per_capita = {}
    for x in range(1, 6 + 1):
        co2_data = json.loads(
            requests.get(
                "http://api.worldbank.org/v2/country/all/indicator/EN.ATM.CO2E.PC?format=json&date=2014&page="
                + str(x)
            ).text
        )[1]
        for c in co2_data:
            country_iso_to_co2_per_capita[c["countryiso3code"]] = c.get("value")
    country_iso_to_gdp_per_capita = {}
    for x in range(1, 6 + 1):
        per_capita_data = json.loads(
            requests.get(
                "http://api.worldbank.org/v2/country/all/indicator/NY.GDP.PCAP.CD?format=json&date=2018&page="
                + str(x)
            ).text
        )[1]
        for c in per_capita_data:
            country_iso_to_gdp_per_capita[c["countryiso3code"]] = c.get("value")
    country_iso_to_total_gdp = {}
    for x in range(1, 6 + 1):
        gdp_data = json.loads(
            requests.get(
                "http://api.worldbank.org/v2/country/all/indicator/NY.GDP.MKTP.CD?format=json&date=2018&page="
                + str(x)
            ).text
        )[1]
        for c in gdp_data:
            country_iso_to_total_gdp[c["countryiso3code"]] = c.get("value")

    country_iso_to_GGE = {}
    for x in range(1, 6 + 1):
        GGE_data = json.loads(
            requests.get(
                "https://api.worldbank.org/v2/country/all/indicator/EN.ATM.GHGT.KT.CE?format=json&date=2012&page="
                + str(x)
            ).text
        )[1]
        for c in GGE_data:
            country_iso_to_GGE[c["countryiso3code"]] = c.get("value")

    country_iso_to_percent_renewable = {}
    for x in range(1, 6 + 1):
        percent_renewable_data = json.loads(
            requests.get(
                "https://api.worldbank.org/v2/country/all/indicator/EG.FEC.RNEW.ZS?format=json&date=2015&page="
                + str(x)
            ).text
        )[1]
        for c in percent_renewable_data:
            country_iso_to_percent_renewable[c["countryiso3code"]] = c.get("value")

    country_iso_to_life_expectancy = {}
    for x in range(1, 6 + 1):
        life_expectancy_data = json.loads(
            requests.get(
                "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.LE00.IN?format=json&date=2017&page="
                + str(x)
            ).text
        )[1]
        for c in life_expectancy_data:
            country_iso_to_life_expectancy[c["countryiso3code"]] = c.get("value")

    res = json.loads(
        requests.get(
            "https://pkgstore.datahub.io/JohnSnowLabs/country-and-continent-codes-list/country-and-continent-codes-list-csv_json/data/c218eebbf2f8545f3db9051ac893d69c/country-and-continent-codes-list-csv_json.json"
        ).text
    )

    # print(res)
    country_iso_to_continent = {}
    for country in res:
        country_iso_to_continent[country["Three_Letter_Country_Code"]] = country[
            "Continent_Code"
        ]

    for c in data_list:
        id = country_iso_to_id.get(c["country_iso"])
        total_GDP = country_iso_to_total_gdp.get(c["country_iso"])
        GDP_per_capita = country_iso_to_gdp_per_capita.get(c["country_iso"])
        CO2_per_capita = country_iso_to_co2_per_capita.get(c["country_iso"])
        GGE = country_iso_to_GGE.get(c["country_iso"])
        percent_renewable = country_iso_to_percent_renewable.get(c["country_iso"])
        average_lifespan = country_iso_to_life_expectancy.get(c["country_iso"])
        continent = country_iso_to_continent.get(c["country_iso"])
        print(continent)
        CO2_div_GDP = None
        if total_GDP is not None and GGE is not None:
            CO2_div_GDP = total_GDP / GGE
        country_data = CountryData(
            continent=continent,
            average_lifespan=average_lifespan,
            CO2_div_GDP=CO2_div_GDP,
            GGE=GGE,
            percent_renewable=percent_renewable,
            GDP_per_capita=GDP_per_capita,
            total_GDP=total_GDP,
            CO2_per_capita=CO2_per_capita,
            id=id,
            population=c["population"],
        )
        db.session.merge(country_data)
        db.session.commit()

    return "added " + str(len(data_list)) + " countries to db"

# filling city_data table using google maps api data and waqi api data
def scrape_city_data(db):
    rows = db.session.query(CityInfo).all()
    for row in rows:
        id = row.id
        country_id = row.country_id
        city_name = row.city_name
        lat = row.lat
        lon = row.lon

        response = json.loads(
            requests.get(
                "https://maps.googleapis.com/maps/api/elevation/json?locations="
                + str(lat)
                + ","
                + str(lon)
                + "&key=AIzaSyCYVY0D8-SfdxSMYG7WQD6jvkHDAJKQ1e4"
            ).text
        )
        elevation = response["results"][0]["elevation"]
        response = json.loads(
            requests.get(
                "https://api.waqi.info/feed/geo:"
                + str(lat)
                + ";"
                + str(lon)
                + "/?token=7c42de2edbca11f43f2a43883a419e9986a71252"
            ).text
        )
        data = response.get("data")
        aqi = None
        iaqi = None
        if data is not None and isinstance(data, dict):
            aqi = data.get("aqi")
            iaqi = data.get("iaqi")

        pm25 = None
        if iaqi is not None and iaqi.get("pm25") is not None:
            pm25 = iaqi.get("pm25").get("v")
        is_threatened = False
        if elevation is not None and elevation < 15.0:
            is_threatened = True
        if not isinstance(aqi, int):
            aqi = None
        city_data = CityData(
            id=id,
            country_id=country_id,
            is_threatened=is_threatened,
            elevation=elevation,
            aqi=aqi,
            pm25=pm25,
        )
        db.session.merge(city_data)
        time.sleep(0.03)
    db.session.commit()
    return "test scrape_city_data"

# filling stat_info table using data from city_data and country_data tables
def scrape_stat_info(db):
    city_cols = [column for column in CityData.__table__.columns]
    city_cols = city_cols[1:]
    j = 0
    for i in range(len(city_cols)):
        type_ = city_cols[i].type
        if issubclass(type(type_), db.Integer) or issubclass(type(type_), db.Float):
            stat_id = j
            stat_table = 1
            stat_table_col = city_cols[i].name
            j = j + 1
            stat_info = StatInfo(
                stat_id=stat_id, stat_table=stat_table, stat_table_col=stat_table_col
            )
            db.session.merge(stat_info)

    country_cols = [column for column in CountryData.__table__.columns]
    country_cols = country_cols[1:]

    for i in range(len(country_cols)):
        type_ = country_cols[i].type
        if issubclass(type(type_), db.Integer) or issubclass(type(type_), db.Float):
            stat_id = j
            stat_table = 0
            stat_table_col = country_cols[i].name
            j = j + 1
            stat_info = StatInfo(
                stat_id=stat_id, stat_table=stat_table, stat_table_col=stat_table_col
            )
            db.session.add(stat_info)

    db.session.commit()


# run cd

# filling stat_data table using data from city_data and country_data tables
def scrape_stat_data(db):
    stats = db.session.query(StatInfo).all()
    for stat in stats:
        talbe_data = None
        if stat.stat_table == 0:
            table_data = db.session.query(CountryData).all()
        else:
            table_data = db.session.query(CityData).all()
        col_name = stat.stat_table_col
        stat_id = stat.stat_id
        high_id = -1
        high_val = None
        low_id = -1
        low_val = None
        mean = 0
        median = 0
        first_quartile = 0
        third_quartile = 0
        std_dev = 0
        num_samples = 0
        pop = 0
        items = []
        for row in table_data:
            val = getattr(row, col_name)
            if val is None:
                continue
            if high_val is None:
                high_val = val
                high_id = row.id
                low_val = val
                low_id = row.id
            if val > high_val:
                high_val = val
                high_id = row.id
            if val < low_val:
                low_val = val
                low_id = row.id
            items.append(val)
            num_samples = num_samples + 1

        mean = statistics.fmean(items)
        median = statistics.median(items)
        std_dev = statistics.pstdev(items)
        quartiles = [q for q in statistics.quantiles(items, n=4)]
        first_quartile = quartiles[0]
        third_quartile = quartiles[2]
        stat_data = StatData(
            stat_id=stat_id,
            high_id=high_id,
            high_val=high_val,
            low_id=low_id,
            low_val=low_val,
            mean=mean,
            median=median,
            first_quartile=first_quartile,
            third_quartile=third_quartile,
            std_dev=std_dev,
            num_samples=num_samples,
            pop=pop,
        )
        db.session.add(stat_data)

    db.session.commit()
    return "hello from scrape stat data"


def get_country_metadata(db, id):
    # get the country metadata for country with id
    country = CountryInfo.query.filter_by(id=id).first()
    print(country.country_name)
    print(country.country_iso)
    return country

# filling city_data table using UN urban development study csv data
def scrape_urban_dev_csv(db):
    import geopy.distance

    city_data = []
    with open("urban_development_compressed.csv", newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            city_data.append(row)

    city_loc_table = {}
    for city in city_data:
        lat = int(round((float(city["BBX_LATMN"]) + float(city["BBX_LATMX"])) / 2))
        long = int(round((float(city["BBX_LONMN"]) + float(city["BBX_LONMX"])) / 2))
        already_present = city_loc_table.get((lat, long), None)
        if already_present is None:
            already_present = []
        already_present.append(city)
        city_loc_table[(lat, long)] = already_present

    db_city_info = []
    db_city_info = db.session.query(CityInfo).all()
    for row in db_city_info:
        lat = int(round(row.lat))
        long = int(round(row.lon))
        search_range = 3
        potential_matches = []
        for x in range(lat - search_range, lat + search_range + 1):
            for y in range(long - search_range, long + search_range + 1):
                located_at_index = city_loc_table.get((x, y), None)
                if located_at_index is not None:
                    for match in located_at_index:
                        potential_matches.append(match)
        if len(potential_matches) > 0:
            lowest_val = 1000000000
            lowest_match = None
            for pm in potential_matches:
                coords_1 = (row.lat, row.lon)
                coords_2 = (
                    (float(pm["BBX_LATMN"]) + float(pm["BBX_LATMX"])) / 2,
                    (float(pm["BBX_LONMN"]) + float(pm["BBX_LONMX"])) / 2,
                )
                distance = geopy.distance.vincenty(coords_1, coords_2).km
                if distance < lowest_val:
                    lowest_val = distance
                    lowest_match = pm

            # lowest_match is now the physically closest city in the dataset to the current city from db
            print(lowest_match)
            cd = CityData(
                id=row.id,
                is_threatened=bool(lowest_match["RISING_OCEAN_RISK"] == "True"),
                un_development_group=lowest_match["DEV_CMI"],
                total_builtup_area=float(lowest_match["B15"]),
                climate_description=lowest_match["E_KG_NM_LST"],
                precipitation_2014_mm=float(lowest_match["E_WR_P_14"]),
                percent_greenness=float(lowest_match["E_GR_AV14"]),
            )
            db.session.merge(cd)
    db.session.commit()

# filling city_data table using UN carbon footprint study csv data
def scrape_carbon_footprint_csv(db):
    import Levenshtein

    city_data = []
    file_str = "GGMCF_top500cities.csv"
    with open(file_str, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # remove accents from city names preemptively to save computation later
            row["unaccented_name"] = remove_accents(
                str(row["Urban Cluster"]).replace('"', "")
            )

            # check for , and / preemptively to save computation later
            row["has_comma"] = False
            row["has_slash"] = False
            if "," in row["unaccented_name"]:
                row["has_comma"] = True
            if "/" in row["unaccented_name"]:
                row["has_slash"] = True

            row["footprint"] = float(row["Footprint/cap (t CO2)"].split("&plusmn;")[0])
            row["gge"] = float(row["Footprint (Mt CO2)"].split("&plusmn;")[0])
            city_data.append(row)

    db_city_info = []
    db_city_info = db.session.query(CityInfo).all()
    for row in db_city_info:
        lowest_val = 1000000000
        lowest_match = None
        db_name = remove_accents(row.city_name)
        for c in city_data:
            name = c["unaccented_name"]
            # look for city names with , and / in them, and test both portions of name
            # some cities are listed with city and district/prefecture separated with those chars
            bad_char = ""
            if c["has_comma"]:
                bad_char = ","
            if c["has_slash"]:
                bad_char = "/"
            if len(bad_char) == 1:
                spl = name.split(bad_char)
                for n in spl:
                    if len(n) > 3:
                        distance = Levenshtein.distance(db_name, n)
                        if distance < lowest_val:
                            lowest_val = distance
                            lowest_match = c
            # use Levenshtein distance to match city names automatically
            distance = Levenshtein.distance(db_name, name)
            if distance < lowest_val:
                lowest_val = distance
                lowest_match = c
        print(
            "db name "
            + db_name
            + " matched to "
            + remove_accents(lowest_match["Urban Cluster"])
        )
        print(lowest_match)
        footprint = lowest_match["footprint"]
        gge = lowest_match["gge"]
        cd = CityData(id=row.id, CO2_per_capita=footprint, GGE=gge)
        db.session.merge(cd)
    db.session.commit()

# fill country_info with iso2 country codes from JohnSnowLabs's 'country-and-continent-codes-list' api data
def scrape_iso2(db):
    res = json.loads(
        requests.get(
            "https://pkgstore.datahub.io/JohnSnowLabs/country-and-continent-codes-list/country-and-continent-codes-list-csv_json/data/c218eebbf2f8545f3db9051ac893d69c/country-and-continent-codes-list-csv_json.json"
        ).text
    )

    iso2_mapping = {}
    iso3_found_set = set()
    for country in res:
        iso2_mapping[country["Three_Letter_Country_Code"]] = country["Two_Letter_Country_Code"]
        iso3_found_set.add(country["Three_Letter_Country_Code"])

    db_country_info = db.session.query(CountryInfo).all()
    for row in db_country_info:
        if row.country_iso in iso3_found_set:
            row.country_two = iso2_mapping[row.country_iso]
            db.session.merge(row)
    db.session.commit()

# remove accented characters from city and country names to help make data more easily searched and filtered
def remove_accents(s):
    accented_string = s.encode("utf-8")
    accented_string = accented_string.decode("utf-8")
    unaccented_string = unidecode.unidecode(accented_string)
    return unaccented_string
