from pandas import DataFrame, read_csv
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel("GHS_STAT_UCDB2015MT_GLOBE_R2019A_V1_1.xls", sheet_name="Data")
column_list = [
    "BBX_LATMN",
    "BBX_LONMN",
    "BBX_LATMX",
    "BBX_LONMX",
    "GCPNT_LAT",
    "GCPNT_LON",
    "XC_ISO_LST",
    "UC_NM_MN",
    "E_BM_NM_LST",
    "EL_AV_ALS",
    "E_KG_NM_LST",
    "GDP15_SM",
    "INCM_CMI",
    "DEV_CMI",
    "BUCAP15",
    "EX_SS_P15",
    "EX_SS_B15",
    "E_WR_T_14",
    "E_WR_T_00",
    "E_WR_T_90",
    "E_WR_P_14",
    "E_KG_NM_LST",
    "BUCAP15",
    "P15",
    "P00",
    "P75",
    "P90",
    "B15",
    "B00",
    "B75",
    "B90",
    "E_GR_AV14",
]
# print(df)

df = df[column_list]

# filter-out cities with population under 150,000
pop = df["P15"] > 150000
df = df[pop]
print(df)

df_base = df

filt = df["EX_SS_P15"] > 0.1
df = df[filt]
filt = df["EX_SS_B15"] > 0.1
df = df[filt]
filt = df["EX_SS_P15"] > (df["EX_SS_P15"].mean() * 0.65)
df = df[filt]
filt = df["EX_SS_B15"] > (df["EX_SS_B15"].mean() * 0.65)

df_base["RISING_OCEAN_RISK"] = filt
df_base["RISING_OCEAN_RISK"] = df_base["RISING_OCEAN_RISK"].fillna(False)

print(df_base)

df = df[filt]

print(df["EX_SS_P15"].mean())
print(df["EX_SS_P15"].median())
print(df["EX_SS_B15"].mean())
print(df["EX_SS_B15"].median())

# df['EX_SS_P15'].plot.hist(bins=20)
# plt.show()
# df['EX_SS_B15'].plot.hist(bins=20)

# plt.show()

df_base.to_csv("urban_development_compressed.csv", index=False)
