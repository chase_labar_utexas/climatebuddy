# from flask_cors import CORS, cross_origin
import json

# import pkgutil
# search_path = ['.'] # set to None to see all modules importable from sys.path
# all_modules = [x[1] for x in pkgutil.iter_modules(path=search_path)]
# print(all_modules)

# wraps json object in a response object containing Access-Control-Allow-Origin header for SSL
def wrap_header(jsonify_obj):
    resp = jsonify_obj
    resp.headers["Access-Control-Allow-Origin"] = "*"
    return resp

# takes city.data and city.info SQLAlchemy objects and extracts fields from them into corresponding API field names from Postman
def city_row_dict(data, info):
    result = {
        "cityID": info.id,
        "name": info.city_name,
        "AQI": data.aqi,
        "population": data.population,
        "country": info.country_id,
        "isRisingSea": data.is_threatened,
        "elevation": data.elevation,
        "GGE": data.GGE,
        "pm25": data.pm25,
        "CO2_per_capita": data.CO2_per_capita,
        "un_development_group": data.un_development_group,
        "total_builtup_area": data.total_builtup_area,
        "climate_description": data.climate_description,
        "precipitation_2014_mm": data.precipitation_2014_mm,
        "percent_greenness": data.percent_greenness,
        "lat": info.lat,
        "lon": info.lon
    }
    return result

# takes country.data and country.info SQLAlchemy objects and extracts fields from them into corresponding API field names from Postman
def country_row_dict(data, info):
    result = {
        "countryID": info.id,
        "name": info.country_name,
        "GDPCapita": data.GDP_per_capita,
        "emissionsCapita": data.CO2_per_capita,
        "GDP": data.total_GDP,
        "hasSignedPCA": data.signed_paris_agreement,
        "avgLifespan": data.average_lifespan,
        "population": data.population,
        "continent": data.continent,
        "HDI": data.hdi,
        "emmissionsGDP": data.CO2_div_GDP,
        "GGE": data.GGE,
        "percentRenewable": data.percent_renewable,
        "iso2": info.country_two
    }
    return result


import os
if __name__ == "__main__" or "gunicorn" in os.environ.get("SERVER_SOFTWARE", ""):

    import requests
    import json
    import flask
    import flask_sqlalchemy
    from flask import jsonify, Flask, redirect, url_for, request, abort

    from models import (
        db,
        CountryInfo,
        CountryData,
        CityInfo,
        CityData,
        StatInfo,
        StatData,
    )
    from init import create_app
    from scrape import scrape_main

    application = create_app()

    stats_map_cached = {}

    @application.route("/", methods=["GET"])
    # @cross_origin()
    def homepage_request():
        return wrap_header(flask.Response("Home!", status=200))

    @application.route("/scrape", methods=["GET"])
    # @cross_origin()
    def scrape_request():
        return wrap_header(flask.Response(scrape_main(db)))

    """
    COUNTRIES
    """

    @application.route("/country", methods=["GET"])
    # @cross_origin()
    def country_request():
        c_id = int(request.args["countryID"])
        data = db.session.query(CountryData).get(c_id)
        info = db.session.query(CountryInfo).get(c_id)
        result = country_row_dict(data, info)
        return wrap_header(jsonify(result))

    @application.route("/countries", methods=["GET"])
    # @cross_origin()
    def all_countries_request():
        result = []

        # store rows in dicts to access matching rows from different tables simultaneously
        all_data = db.session.query(CountryData).all()
        data_dict = {}
        for d in all_data:
            data_dict[d.id] = d

        all_info = db.session.query(CountryInfo).all()
        info_dict = {}
        for i in all_info:
            info_dict[i.id] = i

        for k in data_dict.keys():
            data = data_dict[k]
            info = info_dict[k]
            result.append(country_row_dict(data, info))
        return wrap_header(jsonify(result))

    """
    CITIES
    """

    @application.route("/city", methods=["GET"])
    # @cross_origin()
    def city_request():
        c_id = int(request.args["cityID"])
        data = db.session.query(CityData).get(c_id)
        info = db.session.query(CityInfo).get(c_id)
        result = city_row_dict(data, info)
        return wrap_header(jsonify(result))

    @application.route("/cities", methods=["GET"])
    # @cross_origin()
    def all_cities_request():
        result = []

        # store rows in dicts to access matching rows from different tables simultaneously
        all_data = db.session.query(CityData).all()
        data_dict = {}
        for d in all_data:
            data_dict[d.id] = d

        all_info = db.session.query(CityInfo).all()
        info_dict = {}
        for i in all_info:
            info_dict[i.id] = i

        for k in data_dict.keys():
            data = data_dict[k]
            info = info_dict[k]
            result.append(city_row_dict(data, info))
        return wrap_header(jsonify(result))


    """
    STATS
    """

    @application.route("/stat", methods=["GET"])
    # @cross_origin()
    def stat_request():
        stat_id = int(request.args["statID"])
        data = db.session.query(StatData).get(stat_id)
        info = db.session.query(StatInfo).get(stat_id)
        result = stat_row_dict(data, info, db)
        increment_stat_view_count(data, db)
        return wrap_header(jsonify(result))

    @application.route("/stats", methods=["GET"])
    # @cross_origin()
    def all_stats_request():
        result = []

        # store rows in dicts to access matching rows from different tables simultaneously
        all_data = db.session.query(StatData).all()
        data_dict = {}
        for d in all_data:
            data_dict[d.stat_id] = d

        all_info = db.session.query(StatInfo).all()
        info_dict = {}
        for i in all_info:
            info_dict[i.stat_id] = i

        for k in data_dict.keys():
            data = data_dict[k]
            info = info_dict[k]
            result.append(stat_row_dict(data, info, db))
        return wrap_header(jsonify(result))

    # takes stat.data and stat.info SQLAlchemy objects and extracts fields from them into corresponding API field names from Postman
    # also pulls high and low values for the specific stat from city/country table
    def stat_row_dict(data, info, db):
        lowName = None
        highName = None
        if info.stat_table == 0:
            highName = db.session.query(CountryInfo).get(data.high_id).country_name
            lowName = db.session.query(CountryInfo).get(data.low_id).country_name
        elif info.stat_table == 1:
            highName = db.session.query(CityInfo).get(data.high_id).city_name
            lowName = db.session.query(CityInfo).get(data.low_id).city_name
        result = {
            "statID": info.stat_id,
            "name": info.stat_table_col,
            "isCountry": (not bool(info.stat_table)),
            "high": {"id": data.high_id, "name": highName, "value": data.high_val},
            "low": {"id": data.low_id, "name": lowName, "value": data.low_val},
            "median": data.median,
            "mean": data.mean,
            "q1": data.first_quartile,
            "q3": data.third_quartile,
            "stdDev": data.std_dev,
            "numSamples": data.num_samples,
            "pop": data.pop,
        }
        return result

    @application.route("/stat_map", methods=["GET"])
    # @cross_origin()
    def stats_map_request():
        global stats_map_cached
        result = stats_map_cached
        if len(result) < 1:
            result = {0: {}, 1: {}}
            all_info = db.session.query(StatInfo).all()
            for info in all_info:
                result[info.stat_table][info.stat_table_col] = info.stat_id
            stats_map_cached = result
        return wrap_header(jsonify(result))
    
    # adds 1 to the popularity value for the statistic, which is used to rank statistic popularity relative to other statistics
    def increment_stat_view_count(data, db):
        data.pop += 1
        db.session.merge(data)
        db.session.commit()

    if __name__ == "__main__":
        application.debug = True
        application.run(host="0.0.0.0", port=80)
