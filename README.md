# ClimateBuddy

Names: Chase Labar, Eric Wu, Ewin Zuo, Isaac Castaneda, Kevin Diep

EIDs: crl2534, ew9478, ejz263, icc297, kd24972

Project Leaders: Chase Labar & Ewin Zuo

GitLab IDs: Chase Labar - chase_labar_utexas, Eric Wu - EricWu9478, Ewin Zuo - eztso, Isaac Castaneda - pursin, Kevin Diep - kd24972

Git SHA: b84f308e

GitLab Pipelines: https://gitlab.com/chase_labar_utexas/climatebuddy/pipelines/143092466

Website Link: https://www.climatebuddy.xyz/

Postman Link: https://documenter.getpostman.com/view/10508760/SzYYzxx7?version=latest

Estimated completion time: Chase Labar - 15, Eric Wu - 17, Ewin Zuo - 15, Isaac Castaneda - 14, Kevin Diep - 18

Actual completion time: Chase Labar - 19, Eric Wu - 19, Ewin Zuo - 20, Isaac Castaneda - 22, Kevin Diep - 20
