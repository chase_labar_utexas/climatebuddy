import React, {Component} from 'react';
import { Nav, Navbar, Button} from 'react-bootstrap';
import '../css/parent.css';

class NavBar extends Component {
    render() {
        return (
            // <Navbar expand="lg" bg="dark">
            //     <Navbar.Brand href="/Homepage">ClimateBuddy</Navbar.Brand>
            //     <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            //     <Navbar.Collapse id="responsive-navbar-nav" >
            //         <Nav>
            //             <Nav.Link href="/home">Home</Nav.Link>
            //             <Nav.Link href="/worldview">Worldview</Nav.Link>
            //             <Nav.Link href="/countries">Countries</Nav.Link>
            //             <Nav.Link href="/cities">Cities</Nav.Link>
            //             <Nav.Link href="/about">About</Nav.Link>
            //         </Nav>
            //     </Navbar.Collapse>
            // </Navbar>
            <header>
              <nav className="navbar navbar-expand-lg navbar-dark">
                <div className= "container nav-inner">
                  <img src={require("../img/cloud.png")} className= "header-icon"></img>
                  <a className="navbar-brand" href="/">ClimateBuddy</a>
                  <button className="navbar-toggler" type="button"  data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                      <li className="nav-item">
                        <a className="nav-link" href="/">Home </a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/worldviews">Worldview </a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/countries">Countries</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/cities">Cities</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/about">About</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/visualizations">Visualizations</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </header>
        );
    }
}
export default NavBar;
