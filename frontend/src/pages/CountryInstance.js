import React, { Component } from "react";
import NavBar from "../components/Navbar.js";
import $ from "jquery";
import Popper from "popper.js";
import "../css/countriesinstance.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";

class CountryInstance extends Component {
  constructor() {
    super();
    console.log("in constructor");
    this.state = {
      country: {},
      cities: [],
      stat_map: {},
    };
  }
  componentDidMount() {
    console.log("mounted");
    var id = this.props.match.params.countryID;

    fetch(`https://api.climatebuddy.xyz/cities`)
      .then((response) => response.json())
      .then((response) => {
        var countryID = id;
        fetch(`https://api.climatebuddy.xyz/country?countryID=` + countryID)
          .then((res) => res.json())
          .then((data) => {
            var stuff = [];
            for (let i = 0; i < response.length; i++) {
              if (response[i].country == id) {
                stuff.push(response[i]);
              }
            }
            fetch(`https://api.climatebuddy.xyz/stat_map`)
              .then((r) => r.json())
              .then((stat_data) => {
                this.setState({
                  country: data,
                  cities: stuff,
                  stat_map: stat_data,
                });
                console.log(data);
                console.log(this.state.stat_map);
                console.log(this.state.country["GDP"]);
                console.log(this.state.stat_map["0"]);
              });
          });
      });
  }

  city_links = () => {
    return (
      <>
        {this.state.cities.map((city, index) => (
          <Link to={`/city/${city.cityID}`}>{city.name}, </Link>
        ))}
      </>
    );
  };

  stat_link = (stat_name, display) => {
    var stat_entry = undefined;
    console.log(stat_name + "hi");
    console.log(this.state.stat_map + "hi");
    if (this.state.stat_map["0"])
      var stat_entry = this.state.stat_map["0"][stat_name];
    return (
      <>
        <Link to={`/worldview/${stat_entry}`}>{display}</Link>
      </>
    );
  };

  render() {
    console.log("render");
    var signed_pca = "No";
    if (this.state.country.hasSignedPCA) {
      signed_pca = "Yes";
    }
    for (var key in this.state.country) {
      if (!this.state.country[key]) {
        this.state.country[key] = "N/A";
      }
    }
    var imgsrc =
      "https://maps.googleapis.com/maps/api/staticmap?center=" +
      this.state.country.name +
      "&size=500x250&maptype=terrain&key=AIzaSyA7srUjqGDYWEDzyIGBfb5UxZhfT8woc94";
    return (
      <div>
        <NavBar />
        <div className="container text-center">
          <div className="">
            <p className="head-text m-0">
              <b>{this.state.country.name}</b>
            </p>
            <p class="name text-center">
              {" "}
              <img src={imgsrc} />{" "}
            </p>
          </div>
          <div className="row row-info">
            <div className="col-4">
              <div className="stat">
                <p>{this.stat_link("total_GDP", "GDP")}</p>
                <p className="data">{this.state.country.GDP}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("GDP_per_capita", "GDP Per Capita")}</p>
                <p className="data">{this.state.country.GDPCapita}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("GGE", "Total Greenhouse Gas Emissions")}</p>
                <p className="data">{this.state.country.GGE}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("hdi", "Human Development Index")}</p>
                <p className="data">{this.state.country.HDI}</p>
              </div>
            </div>
            <div className="col-4">
              <div className="stat">
                <p>{this.stat_link("CO2_div_GDP", "GDP per kt CO2")}</p>
                <p className="data">{this.state.country.emmissionsGDP}</p>
              </div>
              <div className="stat">
                <p>Continent</p>
                <p className="data">{this.state.country.continent}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("CO2_per_capita", "CO2 Per Capita")}</p>
                <p className="data">{this.state.country.emissionsCapita}</p>
              </div>
              <div className="stat">
                <p>Cities</p>
                <p className="data">{this.city_links()}</p>
              </div>
            </div>
            <div className="col-4">
              <div className="stat">
                <p>Signed PCA?</p>
                <p className="data">{signed_pca}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("average_lifespan", "Average Lifespan")}</p>
                <p className="data">{this.state.country.avgLifespan}</p>
              </div>
              <div className="stat">
                <p>{this.stat_link("population", "Population")}</p>
                <p className="data">{this.state.country.population}</p>
              </div>
              <div className="stat">
                <p>
                  {this.stat_link(
                    "percent_renewable",
                    "Percent Renewable Energy"
                  )}
                </p>
                <p className="data">{this.state.country.percentRenewable}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CountryInstance;
