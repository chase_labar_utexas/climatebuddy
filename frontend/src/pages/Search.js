import React, { Component } from "react";
import { Button, Card, CardDeck, Nav, Col } from "react-bootstrap";
import NavBar from "../components/Navbar.js";
import queryString from "query-string";

class Search extends Component {
  constructor() {
    super();
    this.state = {
      countries: [],
      cities: [],
      stats: [],

      search: "a",

      section: 1,
    };

    this.cityCardDeck = [];
    this.countryCardDeck = [];
    this.statCardDeck = [];
    this.cityLength = 0;
    this.countryLength = 0;
    this.statLength = 0;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    console.log("mounted");
    var id = this.props.location.search;
    var query = queryString.parse(id);
    var squery = query.q;

    fetch(`https://api.climatebuddy.xyz/cities`)
      .then((response) => response.json())
      .then((response) => {
        fetch(`https://api.climatebuddy.xyz/countries`)
          .then((res) => res.json())
          .then((ctry) => {
            fetch(`https://api.climatebuddy.xyz/stats`)
              .then((r) => r.json())
              .then((stat) => {
                this.setState({
                  search: squery,
                  cities: response,
                  countries: ctry,
                  stats: stat,
                });
                this.updateMain();
                this.setState({ section: 1 });
              });
          });
      });
  }

  updateMain() {
    this.updateCities(this.state.cities, this.state.search);

    this.updateCountries(this.state.countries, this.state.search);

    this.updateStats(this.state.stats, this.state.search);
  }
  updateSection(num) {
    this.setState({ section: num });
  }

  updateCities(cities, search) {
    console.log("cities!");
    var curCities = [];
    var len = search.length;
    if (len != 0) {
      var words = search.split(" ");
      for (let i = 0; i < words.length; i++) {
        len = words[i].length;
        if (len != 0) {
          var keyword = words[i].toLowerCase();
          for (let j = 0; j < cities.length; j++) {
            //console.log(JSON.stringify(cities[j]))
            var instance = cities[j];
            var percent = (instance.percent_greenness * 100).toFixed(2);
            var is_threatened = "no";
            if (instance.isRisingSea) is_threatened = "yes";
            if (
              String(instance.name).toLowerCase().includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.population).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.AQI).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.CO2_per_capita).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(percent).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.GGE).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.country.name).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.climate_description).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(is_threatened).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.pm25).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.precipitation_2014_mm).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.total_builtup_area).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
            else if (
              String(instance.un_development_group).includes(keyword) &&
              !curCities.includes(instance)
            )
              curCities.push(instance);
          }
        }
      }
    } else {
      curCities = cities.slice();
    }
    var deck = this.makeCards(curCities, 1);
    this.cityCardDeck = deck;
  }

  updateCountries(countries, search) {
    console.log("countries!");
    var curCountries = [];
    var len = search.length;
    if (len != 0) {
      var words = search.split(" ");
      for (let i = 0; i < words.length; i++) {
        len = words[i].length;
        if (len != 0) {
          var keyword = words[i].toLowerCase();
          for (let j = 0; j < countries.length; j++) {
            var instance = countries[j];
            var signed_pca = "No";
            if (instance.hasSignedPCA) {
              signed_pca = "Yes";
            }
            var percent = (instance.HDI * 100).toFixed(2);
            var perR = (instance.percentRenewable * 100).toFixed(2);
            if (
              String(instance.name).toLowerCase().includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.population).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.GDP).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.avgLifespan).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(percent).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.continent).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(signed_pca).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(perR).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.GDPCapita).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.emissionsGDP).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.emissionsCapita).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
            else if (
              String(instance.continent).includes(keyword) &&
              !curCountries.includes(instance)
            )
              curCountries.push(instance);
          }
        }
      }
    } else {
      curCountries = countries.slice();
    }
    var deck = this.makeCards(curCountries, 2);
    this.countryCardDeck = deck;
  }

  updateStats(stats, search) {
    console.log("stats!");
    var dict = {
      CO2_per_capita: "CO2/Capita",
      average_lifespan: "Average Lifespan",
      population: "Population",
      percent_renewable: "Percent Renewable",
      aqi: "Air Quality Index",
      pm25: "PM 25",
      country_id: "Country ID",
      CO2_div_GDP: "CO2/GDP",
      GGE: "Greenhouse Gas Emission",
      percent_greenness: "Percent Greenness",
      total_GDP: "Total GDP",
      elevation: "Elevation",
      total_builtup_area: "Total Builtup Area",
      GDP_per_capita: "GDP/Capita",
      hdi: "HDI",
      precipitation_2014_mm: "Precipitation in 2014",
    };
    var curStats = [];
    var len = search.length;
    if (len != 0) {
      var words = search.split(" ");
      for (let i = 0; i < words.length; i++) {
        len = words[i].length;
        if (len != 0) {
          var keyword = words[i].toLowerCase();
          for (let j = 0; j < stats.length; j++) {
            var instance = stats[j];
            var name = dict[instance.name];
            var high_name = undefined;
            var low_name = undefined;
            var high_val = undefined;
            var low_val = undefined;
            if (instance.high) {
              high_name = instance.high;
              high_val = instance.high.value;
              low_name = instance.low;
              low_val = instance.low.value;
            }

            if (
              String(name).toLowerCase().includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(high_name).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(high_val).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(low_name).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(low_val).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.mean).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.median).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.q1).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.q3).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.stdDev).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.numSamples).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
            else if (
              String(instance.pop).includes(keyword) &&
              !curStats.includes(instance)
            )
              curStats.push(instance);
          }
        }
      }
    } else {
      curStats = stats.slice();
    }
    var deck = this.makeCards(curStats, 3);
    this.statCardDeck = deck;
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    // alert('A name was submitted: ' + this.state.value);
    window.location = "/search?q=" + this.state.value;

    event.preventDefault();
  }

  makeCards(array, section) {
    var styles = { width: "18vw", flexBasis: "auto" };
    var rlen = Math.ceil(array.length / 5);
    var dict = {
      CO2_per_capita: "CO2/Capita",
      average_lifespan: "Average Lifespan",
      population: "Population",
      percent_renewable: "Percent Renewable",
      aqi: "Air Quality Index",
      pm25: "PM 25",
      country_id: "Country ID",
      CO2_div_GDP: "CO2/GDP",
      GGE: "Greenhouse Gas Emission",
      percent_greenness: "Percent Greenness",
      total_GDP: "Total GDP",
      elevation: "Elevation",
      total_builtup_area: "Total Builtup Area",
      GDP_per_capita: "GDP/Capita",
      hdi: "HDI",
      precipitation_2014_mm: "Precipitation in 2014",
    };
    let rows = [];
    for (let r = 0; r < rlen; r++) {
      let cols = [];
      for (let c = 0; c < 5; c++) {
        if (array.length <= c + r * 5) {
          break;
        }
        var instance = array[c + r * 5];

        switch (section) {
          case 0:
            break;
          case 1:
            cols.push(
              <Col style={styles}>
                <Card className="instance-box" style={{ width: "17vw" }}>
                  <Nav.Link
                    className="instance-header"
                    href={"/city/" + String(instance.cityID)}
                  >
                    {" "}
                    {instance.name}{" "}
                  </Nav.Link>
                </Card>
              </Col>
            );
            this.cityLength++;
            break;
          case 2:
            cols.push(
              <Col style={styles}>
                <Card className="instance-box" style={{ width: "17vw" }}>
                  <Nav.Link
                    className="instance-header"
                    href={"/country/" + +String(instance.countryID)}
                  >
                    {" "}
                    {instance.name}{" "}
                  </Nav.Link>
                </Card>
              </Col>
            );
            this.countryLength++;
            break;
          case 3:
            var name = dict[instance.name];
            cols.push(
              <Col style={styles}>
                <Card className="instance-box" style={{ width: "17vw" }}>
                  <Nav.Link
                    className="instance-header"
                    href={"/worldview/" + +String(instance.statID)}
                  >
                    {" "}
                    {name}{" "}
                  </Nav.Link>
                </Card>
              </Col>
            );
            this.statLength++;
            break;
          default:
            break;
        }
      }
      rows.push(<br></br>);
      rows.push(<div className="row instance-padding m-auto">{cols}</div>);
    }
    return rows;
  }

  render() {
    var cardDeck = [];
    if (this.state.section == 1) cardDeck = this.cityCardDeck;
    else if (this.state.section == 2) cardDeck = this.countryCardDeck;
    else if (this.state.section == 3) cardDeck = this.statCardDeck;
    console.log(this.cityCardDeck);

    return (
      <div className="App">
        <NavBar />

        <div className="container-fluid text-center">
          <div className="row">
            <form onSubmit={this.handleSubmit} style={{ margin: "auto" }}>
              <label>
                <input
                  type="text"
                  className="searchpage-bar"
                  placeholder="Search here for cities, countries, or statistics! "
                  value={this.state.value}
                  onChange={this.handleChange}
                />
              </label>
            </form>
          </div>
          <Button
            variant="light"
            onClick={(e) => this.updateSection(1)}
            style={{ margin: "2px" }}
          >
            {" "}
            Cities: {this.cityLength}
          </Button>
          <Button
            variant="light"
            onClick={(e) => this.updateSection(2)}
            style={{ margin: "2px" }}
          >
            {" "}
            Countries: {this.countryLength}{" "}
          </Button>
          <Button
            variant="light"
            onClick={(e) => this.updateSection(3)}
            style={{ margin: "2px" }}
          >
            {" "}
            Statistics: {this.statLength}{" "}
          </Button>
        </div>
        <div className="container-fluid text-left">
          <CardDeck>{cardDeck}</CardDeck>
        </div>
      </div>
    );
  }
}

export default Search;
