import React, { Component } from "react";
import BarChart from "../components/BarChart.js";
import NavBar from "../components/Navbar.js";
import Chart from "react-google-charts";

class Visualizations extends Component {
  constructor() {
    super();
    this.state = {
      cities: [],
      countries: [],
      stats: [],
      companies: [],
      articles: [],
      stocks: [],
    };
  }

  componentDidMount() {
    fetch("https://api.climatebuddy.xyz/cities")
      .then((response) => response.json())
      .then((response) => {
        fetch("https://api.climatebuddy.xyz/countries")
          .then((res) => res.json())
          .then((data) => {
            fetch(`https://api.climatebuddy.xyz/stats`)
              .then((r) => r.json())
              .then((stat_data) => {
                fetch("https://dev.stockmeout.com/api/companies")
                  .then((r4) => r4.json())
                  .then((r4) => {
                    fetch("https://dev.stockmeout.com/api/articles")
                      .then((r5) => r5.json())
                      .then((r5) => {
                        fetch("https://dev.stockmeout.com/api/stocks")
                          .then((r6) => r6.json())
                          .then((r6) => {
                            this.setState({
                              cities: response,
                              countries: data,
                              stats: stat_data,
                              companies: r4,
                              articles: r5,
                              stocks: r6,
                            });
                          });
                      });
                  });
              });
          });
      });
  }

  render() {
    var green_render_data = [];
    var emissions_render_data = [];
    var co2gdp_render_data = [];
    var sector_render_data = [];
    var sentiment_render_data = [];
    var stock_render_data = [];
    for (var i = 0; i < this.state.countries.length; i++) {
      if (this.state.countries[i].percentRenewable) {
        green_render_data.push([
          this.state.countries[i].name,
          this.state.countries[i].percentRenewable,
        ]);
      }
      if (this.state.countries[i].GGE)
        emissions_render_data.push([
          this.state.countries[i].name,
          this.state.countries[i].GGE,
        ]);
      if (
        this.state.countries[i].emissionsCapita &&
        this.state.countries[i].GDPCapita
      )
        co2gdp_render_data.push([
          this.state.countries[i].GDPCapita,
          this.state.countries[i].emissionsCapita,
        ]);
    }
    // console.log(JSON.stringify(this.state.companies))
    var temp = {};
    for (var i = 0; i < this.state.companies.length; i++) {
      if (this.state.companies[i].sector) {
        if (this.state.companies[i].sector in temp) {
          temp[this.state.companies[i].sector] =
            temp[this.state.companies[i].sector] + 1;
        } else {
          temp[this.state.companies[i].sector] = 0;
        }
      }
    }
    for (var key in temp) {
      sector_render_data.push([key, temp[key]]);
    }

    temp = {
      "[-1.0, -0.6]": 0,
      "[-0.6, -0.2]": 0,
      "[-0.2, 0.2]": 0,
      "[0.2, 0.6]": 0,
      "[0.6, 1.0]": 0,
    };
    for (var i = 0; i < this.state.articles.length; i++) {
      if (this.state.articles[i].sentiment) {
        var sentiment = this.state.articles[i].sentiment;
        if (sentiment < -0.6) temp["[-1.0, -0.6]"]++;
        else if (sentiment < -0.2) temp["[-0.6, -0.2]"]++;
        else if (sentiment < 0.2) temp["[-0.2, 0.2]"]++;
        else if (sentiment < 0.6) temp["[0.2, 0.6]"]++;
        else if (sentiment < 1.0) temp["[0.6, 1.0]"]++;
      }
    }
    for (var key in temp) {
      sentiment_render_data.push([key, temp[key]]);
    }

    for (var i = 0; i < this.state.stocks.length; i++) {
      if (
        this.state.stocks[i].all_time_low &&
        this.state.stocks[i].all_time_high
      ) {
        stock_render_data.push([
          this.state.stocks[i].all_time_low,
          this.state.stocks[i].all_time_high,
        ]);
      }
    }

    green_render_data.sort((a, b) => b[1] - a[1]);
    green_render_data.unshift(["Country", "Percent Renewable"]);
    green_render_data = green_render_data.slice(0, 51);

    emissions_render_data.sort((a, b) => b[1] - a[1]);
    emissions_render_data.unshift(["Country", "Total Emissions"]);
    co2gdp_render_data.unshift(["GDP / Capita", "CO2 / Capita"]);

    sector_render_data.unshift(["Sector", "# of Companies"]);
    sentiment_render_data.unshift(["Sentiment", "# Articles"]);

    stock_render_data.unshift(["All Time Low", "All Time High"]);
    console.log(stock_render_data);
    return (
      <div className="App">
        <NavBar />
        <p
          style={{ fontSize: "1.5em", textAlign: "center", paddingTop: "30px" }}
        >
          Developer Visualizations
        </p>
        <div>
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"1000px"}
            chartType="BarChart"
            loader={<div>Loading Chart</div>}
            data={green_render_data}
            options={{
              backgroundColor: "#e3e6f1",
              title: "Percent Renewable of Countries",
              chartArea: { width: "50%", height: "80%" },
              hAxis: {
                title: "Percent Greenness",
                minValue: 0,
              },
              vAxis: {
                showTextEvery: 10,
                title: "Country",
              },
            }}
            // For tests
            rootProps={{ "data-testid": "1" }}
          />
        </div>
        <div>
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"1000px"}
            chartType="PieChart"
            loader={<div>Loading Chart</div>}
            data={emissions_render_data}
            options={{
              backgroundColor: "#e3e6f1",
              title: "Global Greenhouse Gas Emissions",
            }}
            rootProps={{ "data-testid": "1" }}
          />
        </div>
        <div>
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"1000px"}
            chartType="ScatterChart"
            loader={<div>Loading Chart</div>}
            data={co2gdp_render_data}
            options={{
              backgroundColor: "#e3e6f1",
              title: "GDP/Capita vs CO2/Capita",
              hAxis: { title: "GDP/Capita", minValue: 0, maxValue: 15 },
              vAxis: { title: "CO2/Capita", minValue: 0, maxValue: 15 },
              legend: "none",
            }}
            rootProps={{ "data-testid": "1" }}
          />
        </div>
        <p
          style={{ fontSize: "1.5em", textAlign: "center", paddingTop: "30px" }}
        >
          StockMeOut Visualizations
        </p>
        <div>
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"1000px"}
            chartType="PieChart"
            loader={<div>Loading Chart</div>}
            data={sector_render_data}
            options={{
              backgroundColor: "#e3e6f1",
              title: "Sector Breakdown",
            }}
            rootProps={{ "data-testid": "1" }}
          />
        </div>
        <Chart
          style={{ margin: "auto" }}
          width={"90vw"}
          height={"1000px"}
          chartType="BarChart"
          loader={<div>Loading Chart</div>}
          data={sentiment_render_data}
          options={{
            backgroundColor: "#e3e6f1",
            title: "Article Sentiment",
            chartArea: { width: "50%", height: "80%" },
            hAxis: {
              title: "# of Articles",
              minValue: 0,
            },
            vAxis: {
              showTextEvery: 10,
              title: "Sentiment",
            },
          }}
          // For tests
          rootProps={{ "data-testid": "1" }}
        />
        <div>
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"1000px"}
            chartType="ScatterChart"
            loader={<div>Loading Chart</div>}
            data={stock_render_data}
            options={{
              backgroundColor: "#e3e6f1",
              title: "All Time Low vs All Time High",
              hAxis: { title: "All Time Low", minValue: 0, maxValue: 15 },
              vAxis: { title: "All Time High", minValue: 0, maxValue: 15 },
              legend: "none",
            }}
            rootProps={{ "data-testid": "1" }}
          />
        </div>
      </div>
    );
  }
}

export default Visualizations;
