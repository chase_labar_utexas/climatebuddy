import React, { Component } from "react";
import {
  Card,
  CardDeck,
  Nav,
  Table,
  Row,
  Col,
  Button,
  DropdownButton,
  Dropdown,
  SplitButton,
} from "react-bootstrap";
import $ from "jquery";
import NavBar from "../components/Navbar.js";
import Popper from "popper.js";
import "../css/worldview.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";
import { MDBDataTable } from "mdbreact";
import { useHistory } from "react-router-dom";

class Worldview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stats: [],
      pageno: 0,
      pagelen: 10,
      res: {},
      curStats: [],
    };
    this.filterBy = -1;

    this.start = "";
    this.end = "";
  }

  componentDidMount() {
    fetch(`https://api.climatebuddy.xyz/stats`)
      .then((response) => response.json())
      .then((response) => {
        this.setState({ stats: response, curStats: response });
        this.updateTable();
      });
  }

  setStart(value) {
    this.start = value;
  }

  setEnd(value) {
    this.end = value;
  }

  setFilter(value, name) {
    //console.log(name + "hi")
    this.filterBy = value;
    this.setState({
      filter: name,
    });
  }

  updateTable() {
    var dict = {
      CO2_per_capita: "CO2/Capita",
      average_lifespan: "Average Lifespan",
      population: "Population",
      percent_renewable: "Percent Renewable",
      aqi: "Air Quality Index",
      pm25: "PM 25",
      country_id: "Country ID",
      CO2_div_GDP: "CO2/GDP",
      GGE: "Greenhouse Gas Emission",
      percent_greenness: "Percent Greenness",
      total_GDP: "Total GDP",
      elevation: "Elevation",
      total_builtup_area: "Total Builtup Area",
      GDP_per_capita: "GDP/Capita",
      hdi: "HDI",
      precipitation_2014_mm: "Precipitation in 2014",
    };
    var curStats = [];
    var noFilter = false;
    var stat = "mean";
    switch (this.filterBy) {
      case 1:
        stat = "mean";
        break;
      case 2:
        stat = "stdDev";
        break;
      case 3:
        stat = "numSamples";
        break;
    }
    if (this.start == "" && this.end == "") noFilter = true;

    for (let i = 0; i < this.state.stats.length; i++) {
      var instance = this.state.stats[i];
      var name = dict[instance.name];
      //console.log(instance.mean)
      var mean = instance.mean.toLocaleString(navigator.language, {
        minimumFractionDigits: 2,
      });
      var std = instance.stdDev.toLocaleString(navigator.language, {
        minimumFractionDigits: 2,
      });
      let location = "/worldview/" + String(instance.statID);

      if (
        (instance[stat] >= this.start && instance[stat] < this.end) ||
        noFilter
      )
        curStats.push({
          statistic: name,
          highestValueCountry: instance.high.name,
          lowestValueCountry: instance.low.name,
          meanOfStatistic: mean,
          standardDeviation: std,
          numberOfSamples: instance.numSamples,
          clickEvent: () => {
            this.props.history.push({ pathname: location });
          },
        });
    }
    this.makeTable(curStats);
  }
  makeTable(curStats) {
    console.log(curStats + "askdjfalsdkf");
    var columns = [
      {
        label: "Statistic",
        field: "statistic",
        sort: "asc",
        width: 1000,
      },
      {
        label: "Highest Value",
        field: "highestValueCountry",
        sort: "asc",
        width: 150,
      },
      {
        label: "Lowest Value",
        field: "lowestValueCountry",
        sort: "asc",
        width: 150,
      },
      {
        label: "Mean of Statistic",
        field: "meanOfStatistic",
        sort: "desc",
        width: 250,
      },
      {
        label: "Standard Deviation",
        field: "standardDeviation",
        sort: "desc",
        width: 200,
      },
      {
        label: "Number of Samples",
        field: "numberOfSamples",
        sort: "desc",
        width: 50,
      },
    ];
    var temp = [];

    if (curStats == null) return;
    console.log(curStats[0]);
    var dict = {
      CO2_per_capita: "CO2/Capita",
      average_lifespan: "Average Lifespan",
      population: "Population",
      percent_renewable: "Percent Renewable",
      aqi: "Air Quality Index",
      pm25: "PM 25",
      country_id: "Country ID",
      CO2_div_GDP: "CO2/GDP",
      GGE: "Greenhouse Gas Emission",
      percent_greenness: "Percent Greenness",
      total_GDP: "Total GDP",
      elevation: "Elevation",
      total_builtup_area: "Total Builtup Area",
      GDP_per_capita: "GDP/Capita",
      hdi: "HDI",
      precipitation_2014_mm: "Precipitation in 2014",
    };

    for (let c = 0; c < curStats.length; c++) {
      if (curStats.length <= c) {
        break;
      }
      var instance = curStats[c];

      var name = dict[instance.name];
      console.log(name);
      console.log(instance.mean);

      if (
        (name == "Population" ||
          name == "CO2/Capita" ||
          name == "Greenhouse Gas Emission") &&
        instance.isCountry == false
      )
        name = name + " (City)";
      temp.push({
        statistic: instance.statistic,
        highestValueCountry: instance.highestValueCountry,
        lowestValueCountry: instance.lowestValueCountry,
        meanOfStatistic: instance.meanOfStatistic,
        standardDeviation: instance.standardDeviation,
        numberOfSamples: instance.numberOfSamples,
        clickEvent: instance.clickEvent,
      });
    }
    var res = {};
    res.columns = columns;
    res.rows = temp;
    this.setState({ curStats: curStats, res: res });
  }

  handleRowClick = (param) => (e) => {
    let history = useHistory();

    history.push("/" + param);
  };

  render() {
    //console.log(this.state.res)
    return (
      <div className="App">
        <NavBar />
        <div className="container-fluid text-center">
          <div className>
            <p className="head-text m-0">
              World View: {this.state.stats.length} Statistics
            </p>
          </div>
          <p className="head-bottom-text">
            Find statistics and see how your area stacks up!
          </p>
          <Row>
            <label style={{ margin: "auto" }}>
              <span style={{ margin: "5px" }}>Filter</span>
              <DropdownButton
                variant="light"
                id="dropdown-basic-button"
                title={this.state.filter}
                style={{ display: "inline-block" }}
              >
                <Dropdown.Item
                  onBlur={(e) => this.setFilter(1, "Mean Of Statistic")}
                >
                  Mean Of Statistic
                </Dropdown.Item>
                <Dropdown.Item
                  onBlur={(e) => this.setFilter(2, "Standard Deviation")}
                >
                  Standard Deviation
                </Dropdown.Item>
                <Dropdown.Item
                  onBlur={(e) => this.setFilter(3, "Number of Samples")}
                >
                  Number of Samples
                </Dropdown.Item>
              </DropdownButton>
              <span style={{ margin: "5px" }}>from</span>
              <input
                type="text"
                name="start"
                onBlur={(e) => this.setStart(e.target.value)}
              />
              <span style={{ margin: "5px" }}>to</span>
              <input
                type="text"
                name="end"
                onBlur={(e) => this.setEnd(e.target.value)}
              />
              <Button
                variant="dark"
                onClick={(e) => this.updateTable()}
                style={{ margin: "20px" }}
              >
                {" "}
                Go{" "}
              </Button>
            </label>
          </Row>
          <MDBDataTable
            bordered
            small
            hover={true}
            theadColor="light-grey"
            tbodyColor="white"
            noBottomColumns
            data={this.state.res}
          />
        </div>
      </div>
    );
  }
}

export default Worldview;
