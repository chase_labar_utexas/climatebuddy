import React, { Component } from "react";
import { Container, Col, Row } from "react-bootstrap";
import NavBar from "../components/Navbar.js";
import "../css/about.css";
import $ from "jquery";
import Popper from "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";

class About extends Component {
  constructor() {
    super();
    this.state = {
      commits: {},
      issues: {},
      tests: {},
    };

    fetch("https://gitlab.com/api/v4/projects/17060602/repository/contributors")
      .then((res) => res.json())
      .then((response) => {
        fetch("https://gitlab.com/api/v4/projects/17060602/issues?per_page=100")
          .then((issues) => issues.json())
          .then((issue_response) => {
            var ewinCount = 0;
            var ericCount = 0;
            var chaseCount = 0;
            var isaacCount = 0;
            var kevinCount = 0;
            var ewinIssueCount = 0;
            var ericIssueCount = 0;
            var chaseIssueCount = 0;
            var isaacIssueCount = 0;
            var kevinIssueCount = 0;

            for (var key in response) {
              if (response[key].name.includes("Ewin")) {
                ewinCount = ewinCount + response[key].commits;
              } else if (response[key].name.includes("Eric")) {
                ericCount = ericCount + response[key].commits;
              } else if (
                response[key].name.includes("pursin") ||
                response[key].name.includes("Isaac")
              ) {
                isaacCount = isaacCount + response[key].commits;
              } else if (response[key].name.includes("Chase")) {
                chaseCount = chaseCount + response[key].commits;
              } else if (response[key].name.includes("Kevin")) {
                kevinCount = kevinCount + response[key].commits;
              }
            }

            var commits = {};

            commits["ewin"] = ewinCount;
            commits["eric"] = ericCount;
            commits["chase"] = chaseCount;
            commits["kevin"] = kevinCount;
            commits["isaac"] = isaacCount;
            commits["total"] =
              ewinCount + ericCount + chaseCount + kevinCount + isaacCount;

            for (var key in issue_response) {
              if (issue_response[key].closed_by) {
                if (issue_response[key].closed_by.name.includes("Ewin")) {
                  ewinIssueCount = ewinIssueCount + 1;
                } else if (
                  issue_response[key].closed_by.name.includes("Eric")
                ) {
                  ericIssueCount = ericIssueCount + 1;
                } else if (
                  issue_response[key].closed_by.name.includes("pursin") ||
                  issue_response[key].closed_by.name.includes("Isaac")
                ) {
                  isaacIssueCount = isaacIssueCount + 1;
                } else if (
                  issue_response[key].closed_by.name.includes("Chase")
                ) {
                  chaseIssueCount = chaseIssueCount + 1;
                } else if (
                  issue_response[key].closed_by.name.includes("Kevin")
                ) {
                  kevinIssueCount = kevinIssueCount + 1;
                }
              }
            }

            var issues = {};
            issues["ewin"] = ewinIssueCount;
            issues["eric"] = ericIssueCount;
            issues["chase"] = chaseIssueCount;
            issues["kevin"] = kevinIssueCount;
            issues["isaac"] = isaacIssueCount;
            issues["total"] =
              ewinIssueCount +
              ericIssueCount +
              chaseIssueCount +
              kevinIssueCount +
              isaacIssueCount;
            var tests = {};
            this.setState({ commits: commits, issues: issues, tests: tests });
            console.log(ewinIssueCount);
          });
      });
  }

  render() {
    return (
      <div className="App">
        <NavBar />
        <div className="container text-center">
          <div className="">
            <p className="head-text m-0"> Data Driven Purpose </p>
            <p className="head-bottom-text">
              {" "}
              Climate Buddy is your buddy that lets you know about climate
              change on a global scale. We're here to bring awareness to climate
              change by making public data more accessible and visually
              appealing to everyday users who aren't necessarily meterologists.
              Our data serves people who have a desire to know how much climate
              change is currently affecting their daily lives depending on where
              they live.{" "}
            </p>
          </div>
        </div>
        <div className="container">
          <div className="row instance-padding">
            <div className="col-4">
              <div className="instance-box">
                <p className="instance-header text-center">
                  {" "}
                  <b>Chase Labar</b>{" "}
                </p>
                <img
                  src={require("../img/chase.jpeg")}
                  alt="Chase Labar"
                  className="prof-pic"
                />
                <p className="profile-text"> Backend / Ops </p>
                <p className="bio-text">
                  {" "}
                  Chase is originally from Las Vegas, and is now a senior in
                  Computer Science at UT. His main software development
                  interests are cloud computing and video game engines. His main
                  hobbies are film, traveling, and scuba diving.{" "}
                </p>
                <p className="profile-text">
                  Commits: {this.state.commits["chase"]}, Issues{" "}
                  {this.state.issues["chase"]}, Tests: 6{" "}
                </p>
              </div>
            </div>
            <div className="col-4">
              <div className="instance-box">
                <p className="instance-header text-center">
                  {" "}
                  <b>Eric Wu</b>{" "}
                </p>
                <img
                  src={require("../img/eric.jpeg")}
                  alt="Eric Wu"
                  className="prof-pic"
                />
                <p className="profile-text"> Frontend </p>
                <p className="bio-text">
                  {" "}
                  Eric is a third-year Computer Science student at UT. He enjoys
                  the practical side of computer science. In his free time, he
                  likes reading, cooking, and playing games.{" "}
                </p>
                <p className="profile-text">
                  Commits: {this.state.commits["eric"]}, Issues{" "}
                  {this.state.issues["eric"]}, Tests: 0{" "}
                </p>
              </div>
            </div>
            <div className="col-4">
              <div className="instance-box">
                <p className="instance-header text-center">
                  {" "}
                  <b>Ewin Zuo</b>{" "}
                </p>
                <img
                  src={require("../img/ewin.jpeg")}
                  alt="Ewin Zuo"
                  className="prof-pic"
                />
                <p className="profile-text"> Full Stack </p>
                <p className="bio-text">
                  {" "}
                  Ewin is a third-year Computer Science and Mathematics student
                  at UT. He's interested in systems, programming languages, and
                  computer graphics. In his free time he likes to travel and
                  play basketball.{" "}
                </p>
                <p className="profile-text">
                  Commits: {this.state.commits["ewin"]}, Issues{" "}
                  {this.state.issues["ewin"]}, Tests: 10{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="row instance-padding">
            <div className="col-2"></div>
            <div className="col-4">
              <div className="instance-box">
                <p className="instance-header text-center">
                  {" "}
                  <b>Isaac Castaneda</b>{" "}
                </p>
                <img
                  src={require("../img/isaac.jpeg")}
                  alt="Isaac Castaneda"
                  className="prof-pic"
                />
                <p className="profile-text"> Frontend </p>
                <p className="bio-text">
                  {" "}
                  Isaac is a second-year Computer Science student. He enjoys the
                  theoretical side, spending time on algorithms and
                  computations. Outside of CS, he likes to write stories, play
                  video games, and watch more anime than he would like to admit.{" "}
                </p>
                <p className="profile-text">
                  Commits: {this.state.commits["isaac"]}, Issues{" "}
                  {this.state.issues["isaac"]}, Tests: 0{" "}
                </p>
              </div>
            </div>

            <div className="col-4">
              <div className="instance-box">
                <p className="instance-header text-center">
                  {" "}
                  <b>Kevin Diep</b>{" "}
                </p>
                <img
                  src={require("../img/kevin.jpeg")}
                  alt="Kevin Diep"
                  className="prof-pic"
                />
                <p className="profile-text"> Frontend </p>
                <p className="bio-text">
                  {" "}
                  Kevin is a senior studying Computer Science at the University
                  of Texas at Austin. When he's not coding, he likes to workout
                  and watch anime.{" "}
                </p>
                <p className="profile-text">
                  Commits: {this.state.commits["kevin"]}, Issues{" "}
                  {this.state.issues["kevin"]}, Tests: 8{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="container text-center">
          <div className="">
            <p className="head-text m-0">
              {" "}
              <b>Repository Statistics</b>{" "}
            </p>
            <p className="head-bottom-text">
              Commits: {this.state.commits["total"]}, Issues{" "}
              {this.state.issues["total"]}, Tests: 24{" "}
            </p>
          </div>
        </div>

        <div className="container text-center">
          <div className="">
            <p className="head-text m-0">
              {" "}
              <b>Tools</b>{" "}
            </p>
            <div className="row instance-padding">
              <div className="col-4">
                <div className="instance-box">
                  <img src={require("../img/atom.png")} className="icon-pic" />
                  <br />
                  <a
                    href="https://teletype.atom.io/"
                    className="head-bottom-text"
                  >
                    {" "}
                    Atom Teletype
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img src={require("../img/react.png")} className="icon-pic" />
                  <br />
                  <a href="https://reactjs.org/" className="head-bottom-text">
                    {" "}
                    React
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img src={require("../img/aws.png")} className="icon-pic" />
                  <br />
                  <a
                    href="https://aws.amazon.com/"
                    className="head-bottom-text"
                  >
                    {" "}
                    Amazon Web Services
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img
                    src={require("../img/gitlab.png")}
                    className="icon-pic"
                  />
                  <br />
                  <a href="https://gitlab.com/" className="head-bottom-text">
                    Gitlab
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img src={require("../img/slack.png")} className="icon-pic" />
                  <br />
                  <a href="https://slack.com/" className="head-bottom-text">
                    {" "}
                    Slack
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img
                    src={require("../img/postman.png")}
                    className="icon-pic"
                  />
                  <br />
                  <a
                    href="https://www.postman.com/"
                    className="head-bottom-text"
                  >
                    {" "}
                    Postman
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="container text-center"
          style={{ paddingBottom: "50px" }}
        >
          <div className="">
            <p className="head-text m-0">
              {" "}
              <b>Our Links</b>{" "}
            </p>
            <div className="row instance-padding">
              <div className="col-2"></div>
              <div className="col-4">
                <div className="instance-box">
                  <img
                    src={require("../img/gitlab.png")}
                    className="icon-pic"
                  />
                  <br />
                  <a
                    className="head-bottom-text"
                    href="https://gitlab.com/chase_labar_utexas/climatebuddy"
                  >
                    {" "}
                    Gitlab Climate Buddy
                  </a>
                  <br />
                </div>
              </div>
              <div className="col-4">
                <div className="instance-box">
                  <img
                    src={require("../img/postman.png")}
                    className="icon-pic"
                  />
                  <br />
                  <a
                    className="head-bottom-text"
                    href="https://documenter.getpostman.com/view/10508760/SzYYzxx7?version=latest"
                  >
                    {" "}
                    Postman{" "}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
