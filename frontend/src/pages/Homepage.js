import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import NavBar from "../components/Navbar.js";
import $ from "jquery";
import Popper from "popper.js";
import "../css/index.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    // alert('A name was submitted: ' + this.state.value);
    window.location = "/search?q=" + this.state.value;

    event.preventDefault();
  }

  // Search here for cities, countries, or statistics!
  render() {
    return (
      <div className="HomeApp">
        <NavBar />
        <div
          id="splash-jumbotron"
          className="jumbotron d-flex align-items-center"
        >
          <div className="container text-center">
            <div className="bottom-border">
              <p className="top-text m-0">
                Welcome to your <b>Climate Buddy!</b>
              </p>
            </div>
            <div>
              <p className="bottom-text" style={{ paddingTop: "10px" }}>
                "One of my eco-friendly hotties asked how I felt about climate
                change telling me that no one was listening to her, so I asked
                her, 'What can we do to help?!'"
              </p>
              <p className="bottom-text"> --Megan Thee Stallion </p>
              <br />
            </div>
            <div className="row">
              <form
                onSubmit={this.handleSubmit}
                style={{ margin: "auto", display: "flex" }}
              >
                <div>
                  <img
                    src={require("../img/magnifier.png")}
                    className="search-icon"
                    onClick={this.handleSubmit}
                  />
                </div>
                <label>
                  <input
                    type="text"
                    className="bigsearch"
                    placeholder="Search here for cities, countries, or statistics! "
                    value={this.state.value}
                    onChange={this.handleChange}
                  />
                </label>
              </form>
            </div>
            <div className="row preview-padding">
              <div className="col-4">
                <div className="preview-box">
                  <p className="preview-header">
                    {" "}
                    <b>Worldview</b>{" "}
                  </p>
                  <p className="preview-text ">
                    {" "}
                    See the world as a whole. Each country has its role in
                    preventing climate change. See who's doing the worst and who
                    is the best.{" "}
                  </p>
                  <a
                    href="/worldviews"
                    className="btn btn-outline-primary learn-more-button"
                    role="button"
                  >
                    See Worldview
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="preview-box">
                  <p className="preview-header">
                    {" "}
                    <b>Countries</b>{" "}
                  </p>
                  <p className="preview-text">
                    {" "}
                    See your country's statistics. Whether it be CO2 emissions
                    or quality of life, we have it all. Compare yourself to
                    other countries.{" "}
                  </p>
                  <a
                    href="/countries"
                    className="btn btn-outline-primary learn-more-button"
                    role="button"
                  >
                    See Countries
                  </a>
                </div>
              </div>
              <div className="col-4">
                <div className="preview-box">
                  <p className="preview-header">
                    {" "}
                    <b>Cities</b>{" "}
                  </p>
                  <p className="preview-text" style={{ marginBottom: "50px" }}>
                    {" "}
                    See how your individual city stacks up against its
                    neighbors, or even from a city across the globe. Think of
                    changes you can make locally to help fight climate change.
                  </p>
                  <a
                    href="/cities"
                    className="btn btn-outline-primary learn-more-button"
                    role="button"
                  >
                    See Cities
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homepage;
