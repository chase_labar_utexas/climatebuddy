import React, { Component } from "react";
import NavBar from "../components/Navbar.js";
import $ from "jquery";
import Popper from "popper.js";
import "../css/countriesinstance.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import Chart from "react-google-charts";

class WorldviewInstance extends Component {
  constructor() {
    super();
    console.log("in constructor");
    this.state = {
      stat: {},
      all_samples: [["Country", "Population"]],
    };

    this.dict = {
      CO2_per_capita: "CO2/Capita",
      average_lifespan: "Average Lifespan",
      population: "Population",
      percent_renewable: "Percent Renewable",
      aqi: "Air Quality Index",
      pm25: "PM 25",
      country_id: "Country ID",
      CO2_div_GDP: "CO2/GDP",
      GGE: "Greenhouse Gas Emission",
      percent_greenness: "Percent Greenness",
      total_GDP: "Total GDP",
      elevation: "Elevation",
      total_builtup_area: "Total Builtup Area",
      GDP_per_capita: "GDP/Capita",
      hdi: "HDI",
      precipitation_2014_mm: "Precipitation in 2014",
    };
    this.tableToAPI = {
      CO2_per_capita: "emissionsCapita",
      average_lifespan: "avgLifespan",
      population: "population",
      percent_renewable: "percentRenewable",
      aqi: "AQI",
      pm25: "pm25",
      country_id: "countryID",
      CO2_div_GDP: "emmissionsGDP",
      GGE: "GGE",
      percent_greenness: "precent_greenness",
      total_GDP: "GDP",
      elevation: "elevation",
      total_builtup_area: "total_builtup_area",
      GDP_per_capita: "GDPCapita",
      hdi: "HDI",
      precipitation_2014_mm: "precipitation_2014_mm",
    };

    this.countryTableToAPI = {
      CO2_per_capita: "emissionsCapita",
      average_lifespan: "avgLifespan",
      population: "population",
      percent_renewable: "percentRenewable",
      CO2_div_GDP: "emmissionsGDP",
      GGE: "GGE",
      total_GDP: "GDP",
      elevation: "elevation",
      GDP_per_capita: "GDPCapita",
      hdi: "HDI",
    };

    this.cityTableToAPI = {
      country_id: "country",
      population: "population",
      elevation: "elevation",
      aqi: "AQI",
      pm25: "pm25",
      GGE: "GGE",
      CO2_per_capita: "CO2_per_capita",
      total_builtup_area: "total_builtup_area",
      precipitation_2014_mm: "precipitation_2014_mm",
      percent_greenness: "percent_greenness",
    };
  }
  componentDidMount() {
    var id = this.props.match.params.statID;
    console.log(id);
    fetch(`https://api.climatebuddy.xyz/stat?statID=` + id)
      .then((response) => response.json())
      .then((response) => {
        if (response.isCountry) {
          fetch("https://api.climatebuddy.xyz/countries")
            .then((res) => res.json())
            .then((r) => {
              var temp = [];

              for (var i = 0; i < r.length; i++) {
                if (r[i][this.countryTableToAPI[response.name]] != null) {
                  temp.push([
                    r[i].name,
                    1,
                    r[i][this.countryTableToAPI[response.name]],
                  ]);
                }
              }
              temp.sort(function (a, b) {
                return a[2] - b[2];
              });
              for (var i = 0; i < temp.length; i++) {
                var str = temp;
                str[i][1] = ((i + 1) / temp.length) * 100;
              }
              temp.unshift(["Country", "Percentile", this.dict[response.name]]);

              this.setState({ stat: response, all_samples: temp });
            });
        } else {
          fetch("https://api.climatebuddy.xyz/cities")
            .then((res) => res.json())
            .then((r) => {
              var temp = [];

              for (var i = 0; i < r.length; i++) {
                if (r[i][this.cityTableToAPI[response.name]] != null) {
                  temp.push([
                    r[i].lat,
                    r[i].lon,
                    r[i].name,
                    r[i][this.cityTableToAPI[response.name]],
                  ]);
                }
              }

              temp.unshift([
                "Latitude",
                "Lontitude",
                "Name",
                this.dict[response.name],
              ]);

              console.log(JSON.stringify(temp));

              this.setState({ stat: response, all_samples: temp });
            });
        }
      });
  }

  get_link = (stat) => {
    if (this.state.stat.isCountry) {
      return (
        <>
          <Link to={`/country/${stat.id}`}>{stat.name}</Link>
        </>
      );
    }

    return (
      <>
        <Link to={`/city/${stat.id}`}>{stat.name}</Link>
      </>
    );
  };
  render() {
    var high_name = undefined;
    var low_name = undefined;
    var high_val = undefined;
    var low_val = undefined;
    if (this.state.stat.high) {
      high_name = this.get_link(this.state.stat.high);
      high_val = this.state.stat.high.value;
      low_name = this.get_link(this.state.stat.low);
      low_val = this.state.stat.low.value;
    }
    var name = this.dict[this.state.stat.name];
    var country = "City";
    var options = {
      colorAxis: { colors: ["#ab3131", "#31ab31", "#3131ab"] },
      backgroundColor: "#e3e6f1",
      datalessRegionColor: "#f8bbd0",
      defaultColor: "#f5f5f5",
      magnifyingGlass: { enable: true, zoomFactor: 7.5 },
    };
    if (this.state.stat.isCountry) {
      country = "Country";
    } else if (
      name == "Population" ||
      name == "CO2/Capita" ||
      name == "Greenhouse Gas Emission"
    ) {
      name = name + " (City)";
      options["displayMode"] = "markers";
    } else {
      options["displayMode"] = "markers";
    }

    return (
      <div>
        <NavBar />
        <div className="container text-center">
          <div className="">
            <p className="head-text m-0">
              <b>{name}</b>
            </p>
          </div>
          <div className="row instance-padding">
            <Chart
              style={{ margin: "auto" }}
              width={"100%"}
              height={"100%"}
              chartType="GeoChart"
              data={this.state.all_samples}
              // Note: you will need to get a mapsApiKey for your project.
              // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
              mapsApiKey="AIzaSyCi9Pe9T-LqqMiEZqYJty_AoBEV65eZRL0"
              rootProps={{ "data-testid": "1" }}
              options={options}
            />
          </div>
          <div id="city-instance" class="row instance-padding">
            <div class="col-12" data-sort="2">
              <div class="instance-box">
                <table>
                  <tr>
                    <th>Highest</th>
                    <td>{high_name}</td>
                  </tr>
                  <tr>
                    <th>Highest Value</th>
                    <td>{high_val}</td>
                  </tr>
                  <tr>
                    <th>Lowest</th>
                    <td>{low_name}</td>
                  </tr>
                  <tr>
                    <th>Lowest Value</th>
                    <td>{low_val}</td>
                  </tr>
                  <tr>
                    <th>Country or City Statistic</th>
                    <td>{country}</td>
                  </tr>
                  <tr>
                    <th>Mean of Stat</th>
                    <td>{this.state.stat.mean}</td>
                  </tr>
                  <tr>
                    <th>Median of Stat</th>
                    <td>{this.state.stat.median}</td>
                  </tr>
                  <tr>
                    <th>First Quartile</th>
                    <td>{this.state.stat.q1}</td>
                  </tr>
                  <tr>
                    <th>Third Quartile</th>
                    <td>{this.state.stat.q3}</td>
                  </tr>
                  <tr>
                    <th>Standard Deviation</th>
                    <td>{this.state.stat.stdDev}</td>
                  </tr>
                  <tr>
                    <th>Number of Samples</th>
                    <td>{this.state.stat.numSamples}</td>
                  </tr>
                  <tr>
                    <th>Popularity</th>
                    <td>{this.state.stat.pop}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WorldviewInstance;
