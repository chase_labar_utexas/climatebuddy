import React, { Component } from "react";
import { Table } from "react-bootstrap";
import $ from "jquery";
import NavBar from "../components/Navbar.js";
import Popper from "popper.js";
import "../css/cityinstance.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";

class CityInstance extends Component {
  constructor() {
    super();

    this.state = {
      country: {},
      city: {},
      stat_map: {},
    };
  }

  componentDidMount() {
    var id = this.props.match.params.cityID;

    fetch(`https://api.climatebuddy.xyz/city?cityID=` + id)
      .then((response) => response.json())
      .then((response) => {
        var countryID = response.country;
        fetch(`https://api.climatebuddy.xyz/country?countryID=` + countryID)
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            console.log(response);
            console.log(this.state.country);

            fetch(`https://api.climatebuddy.xyz/stat_map`)
              .then((r) => r.json())
              .then((stat_data) => {
                this.setState({
                  country: data,
                  city: response,
                  stat_map: stat_data,
                });
              });
          });
      });
  }

  stat_link = (stat_name, display) => {
    var stat_entry = undefined;
    if (this.state.stat_map["1"])
      var stat_entry = this.state.stat_map["1"][stat_name];
    return (
      <>
        <Link to={`/worldview/${stat_entry}`}>{display}</Link>
      </>
    );
  };

  render() {
    var percent_greenness = this.state.city.percent_greenness * 100.0;
    var is_threatened = "No";
    if (this.state.city.isRisingSea) {
      is_threatened = "Yes";
    }
    var country_url = "/country/" + this.state.city.country;
    var imgsrc =
      "https://maps.googleapis.com/maps/api/staticmap?center=" +
      this.state.city.name +
      "," +
      this.state.country.name +
      "&size=500x250&maptype=terrain&key=AIzaSyA7srUjqGDYWEDzyIGBfb5UxZhfT8woc94";
    var places_photo_src =
      "https://s3.us-east-2.amazonaws.com/climate.buddy.data/city_photos/" +
      this.state.city.cityID +
      ".jpg";

    return (
      <div>
        <NavBar />
        <div class="container city-name main-container">
          <p class="name text-center">
            <b>
              {this.state.city.name},{" "}
              <Link to={country_url}>{this.state.country.name}</Link>
            </b>
          </p>
          <p class="name text-center">
            {" "}
            <img src={imgsrc} /> <img src={places_photo_src} />{" "}
          </p>
          <div
            id="city-instance"
            class="row instance-padding justify-content-center"
          >
            <div class="col-6" data-sort="1">
              <div class="instance-box" style={{ marginBottom: "30px" }}>
                <table>
                  <tr>
                    <th>{this.stat_link("aqi", "Air Quaity Index")}</th>
                    <td>{this.state.city.AQI}</td>
                  </tr>
                  <tr>
                    <th>
                      {this.stat_link("CO2_per_capita", "Emissions per Capita")}
                    </th>
                    <td>{this.state.city.CO2_per_capita}</td>
                  </tr>
                  <tr>
                    <th>Description</th>
                    <td>{this.state.city.climate_description}</td>
                  </tr>
                  <tr>
                    <th>Country</th>
                    <td>{this.state.country.name}</td>
                  </tr>
                  <tr>
                    <th>{this.stat_link("GGE", "Total Emissions")}</th>
                    <td>{this.state.city.GGE}</td>
                  </tr>
                  <tr>
                    <th>Threatened by Rising Sea?</th>
                    <td>{is_threatened}</td>
                  </tr>
                  <tr>
                    <th>
                      {this.stat_link("percent_greenness", "Percent Greenness")}
                    </th>
                    <td>{percent_greenness}</td>
                  </tr>
                  <tr>
                    <th>{this.stat_link("pm25", "PM 2.5")}</th>
                    <td>{this.state.city.pm25}</td>
                  </tr>
                  <tr>
                    <th>{this.stat_link("population", "Population")}</th>
                    <td>{this.state.city.population}</td>
                  </tr>
                  <tr>
                    <th>
                      {this.stat_link(
                        "precipitation_2014_mm",
                        "Precipitation (2014)"
                      )}
                    </th>
                    <td>{this.state.city.precipitation_2014_mm} mm</td>
                  </tr>
                  <tr>
                    <th>
                      {this.stat_link(
                        "total_builtup_area",
                        "Total Builtup Area"
                      )}
                    </th>
                    <td>{this.state.city.total_builtup_area}</td>
                  </tr>

                  <tr>
                    <th>
                      {this.stat_link(
                        "un_development_group",
                        "UN Development Group"
                      )}
                    </th>
                    <td>{this.state.city.un_development_group}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <p class="name text-center"> </p>
        </div>
      </div>
    );
  }
  // <canvas id="popChart"></canvas>
}

export default CityInstance;
