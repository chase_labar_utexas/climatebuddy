import React, { Component } from "react";
import {
  Card,
  CardDeck,
  Nav,
  Table,
  Col,
  Button,
  DropdownButton,
  Dropdown,
  SplitButton,
} from "react-bootstrap";
import $ from "jquery";
import NavBar from "../components/Navbar.js";
import Popper from "popper.js";
import "../css/cities.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";

class Cities extends Component {
  constructor() {
    super();
    this.state = {
      cities: [],
      pageno: 0,
      pagelen: 20,
      filter: "Filter By",
      sort: "Sort By",
      curCities: [],
    };

    this.search = "";
    this.filterBy = -1;

    this.start = "";
    this.end = "";
    this.sortBy = -1;

    this.order = 0;
  }

  componentDidMount() {
    fetch(`https://api.climatebuddy.xyz/cities`)
      .then((response) => response.json())
      .then((response) => {
        this.setState({
          cities: response,
          curCities: response,
        });
      });
  }

  updateCities() {
    this.state.curCities = [];

    // SEARCHING
    var len = this.search.length;
    if (len != 0) {
      var words = this.search.split(" ");
      for (let i = 0; i < words.length; i++) {
        len = words[i].length;
        if (len != 0) {
          var keyword = words[i].toLowerCase();
          for (let j = 0; j < this.state.cities.length; j++) {
            var instance = this.state.cities[j];
            var percent = (instance.percent_greenness * 100).toFixed(2);
            if (
              String(instance.name).toLowerCase().includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
            else if (
              String(instance.population).includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
            else if (
              String(instance.AQI).includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
            else if (
              String(instance.CO2_per_capita).includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
            else if (
              String(percent).includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
            else if (
              String(instance.GGE).includes(keyword) &&
              !this.state.curCities.includes(instance)
            )
              this.state.curCities.push(instance);
          }
        }
      }
    } else {
      this.state.curCities = this.state.cities.slice();
    }

    // FILTERING
    var tempCities = [];
    if (this.start != "" && this.end != "") {
      for (let i = 0; i < this.state.curCities.length; i++) {
        var instance = this.state.curCities[i];
        switch (this.filterBy) {
          case 1:
            if (
              instance.population >= this.start &&
              instance.population < this.end
            )
              tempCities.push(instance);
            break;
          case 2:
            if (instance.AQI >= this.start && instance.AQI < this.end)
              tempCities.push(instance);
            break;
          case 3:
            if (
              instance.CO2_per_capita >= this.start &&
              instance.CO2_per_capita < this.end
            )
              tempCities.push(instance);
            break;
          case 4:
            var percent = (instance.percent_greenness * 100).toFixed(2);
            if (Number(percent) >= this.start && Number(percent) < this.end)
              tempCities.push(instance);
            break;
          case 5:
            if (instance.GGE >= this.start && instance.GGE < this.end)
              tempCities.push(instance);
            break;
          default:
            tempCities.push(instance);
            break;
        }
      }
      this.state.curCities = tempCities;
    }

    // SORTING
    if (this.sortBy != -1) {
      //console.log(this.state.curCities.length)
      this.state.curCities = this.quickSortAsc(
        this.state.curCities,
        this.order
      );
      //console.log(this.order)
      //console.log(this.state.curCities.length)
    }

    this.setState({
      curCities: this.state.curCities,
      pageno: 0,
    });

    this.makeCards();
  }

  quickSortAsc(array, order) {
    if (array.length <= 1) {
      return array;
    } else {
      var multiplier = 1;
      if (order == 1) {
        multiplier = -1;
      }

      var left = [];
      var right = [];
      var newArray = [];
      var pivot = array.pop();
      var length = array.length;

      for (var i = 0; i < length; i++) {
        var instance = array[i];
        switch (this.sortBy) {
          case 1:
            //console.log(pivot)
            if (
              multiplier * instance.population <=
              multiplier * pivot.population
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 2:
            if (multiplier * instance.AQI <= multiplier * pivot.AQI) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 3:
            if (
              multiplier * instance.CO2_per_capita <=
              multiplier * pivot.CO2_per_capita
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 4:
            if (
              multiplier * instance.percent_greenness <=
              multiplier * pivot.percent_greenness
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 5:
            if (multiplier * instance.GGE <= multiplier * pivot.GGE) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
        }
      }
      return newArray.concat(
        this.quickSortAsc(left, order),
        pivot,
        this.quickSortAsc(right, order)
      );
    }
  }

  componentDidMount() {
    fetch(`https://api.climatebuddy.xyz/cities`)
      .then((response) => response.json())
      .then((response) => {
        this.setState({
          cities: response,
          curCities: response,
        });
      });
  }

  makeCards() {
    var styles = { width: "18vw", flexBasis: "auto" };

    var pageof = this.state.pagelen * this.state.pageno;
    let rows = [];
    for (let r = 0; r < 4; r++) {
      let cols = [];
      for (let c = 0; c < 5; c++) {
        if (this.state.curCities.length <= pageof + c + r * 5) {
          break;
        }
        var instance = this.state.curCities[pageof + c + r * 5];
        var percent = (instance.percent_greenness * 100).toFixed(2);
        var pop = instance.population.toLocaleString(navigator.language, {
          minimumFractionDigits: 2,
        });
        cols.push(
          <Col style={styles}>
            <Card className="instance-box" style={{ width: "17vw" }}>
              <img
                src={
                  "https://s3.us-east-2.amazonaws.com/climate.buddy.data/city_icons_140/" +
                  String(instance.cityID) +
                  ".jpg"
                }
                style={{ margin: ".5em 1em 0" }}
              ></img>
              <Nav.Link
                className="instance-header"
                href={"/city/" + String(instance.cityID)}
              >
                {" "}
                {instance.name}{" "}
              </Nav.Link>
              <Card.Body>
                <Card.Text> Population: {pop} </Card.Text>
                <Card.Text> Air Quality Index: {instance.AQI} </Card.Text>
                <Card.Text> CO2/Capita: {instance.CO2_per_capita} </Card.Text>
                <Card.Text> Percent Greenery: {percent} </Card.Text>
                <Card.Text> Greenhouse Gas Emission: {instance.GGE} </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        );
      }

      rows.push(<br></br>);
      rows.push(<div className="row instance-padding m-auto">{cols}</div>);
    }
    return rows;
  }

  getPage(i) {
    var totpage = Math.ceil(this.state.curCities.length / this.state.pagelen);
    if (i >= 0 && i < totpage) {
      this.setState({ pageno: i });
      this.makeCards();
    }
  }

  prevPage() {
    if (this.state.pageno > 0) {
      this.setState({ pageno: this.state.pageno - 1 });
      this.makeCards();
    }
  }

  nextPage() {
    var totpage = Math.ceil(this.state.curCities.length / this.state.pagelen);
    if (this.state.pageno < totpage - 1) {
      this.setState({ pageno: this.state.pageno + 1 });
      this.makeCards();
    }
  }

  setAsc() {
    this.order = 0;
  }

  setDesc() {
    this.order = 1;
  }

  setFilter(value, name) {
    this.filterBy = value;
    this.setState({
      filter: name,
    });
  }

  setSort(value, name) {
    this.sortBy = value;
    this.setState({
      sort: name,
    });
  }

  setSearch(value) {
    this.search = value;
  }

  setStart(value) {
    this.start = value;
  }

  setEnd(value) {
    this.end = value;
  }

  buildNav() {
    var totpage = Math.floor(this.state.curCities.length / this.state.pagelen);
    var curPage = this.state.pageno;
    var cols = [];
    cols.push(
      <Button variant="light" onClick={(e) => this.getPage(0)}>
        &lt;&lt;
      </Button>
    );
    cols.push(
      <Button variant="light" onClick={(e) => this.prevPage()}>
        &lt;
      </Button>
    );
    var offset = 2;

    if (curPage - 2 >= 0) {
      cols.push(
        <Button variant="light" onClick={(e) => this.getPage(curPage - 2)}>
          {curPage - 1}
        </Button>
      );
    } else offset++;
    if (curPage - 1 >= 0) {
      cols.push(
        <Button variant="light" onClick={(e) => this.getPage(curPage - 1)}>
          {curPage}
        </Button>
      );
    } else offset++;
    if (curPage >= 0) {
      cols.push(
        <Button variant="dark" onClick={(e) => this.getPage(curPage)}>
          {curPage + 1}
        </Button>
      );
    }

    for (var i = 1; i <= offset; i++) {
      cols.push(this.createCol(curPage + i));
    }

    cols.push(
      <Button variant="light" onClick={(e) => this.nextPage()}>
        &gt;
      </Button>
    );

    cols.push(
      <Button
        variant="light"
        onClick={(e) =>
          this.getPage(
            Math.ceil(this.state.curCities.length / this.state.pagelen) - 1
          )
        }
      >
        &gt;&gt;
      </Button>
    );
    return cols;
  }

  createCol(i) {
    return (
      <Button variant="light" value={i} onClick={(e) => this.getPage(i)}>
        {i + 1}
      </Button>
    );
  }

  render() {
    return (
      <div className="App">
        <NavBar />
        <div className="container-fluid text-center">
          <div className>
            <p className="head-text m-0">
              Cities {this.state.curCities.length}
            </p>
          </div>
          <p className="head-bottom-text">
            Find your city and see how it stacks up!
          </p>
          <div>{this.buildNav()}</div>
          <div>
            <label style={{ margin: "20px" }}>
              {" "}
              Search:{" "}
              <input
                type="text"
                name="search"
                onBlur={(e) => this.setSearch(e.target.value)}
              />
            </label>
            <label>
              <DropdownButton
                variant="light"
                id="dropdown-basic-button"
                title={this.state.filter}
                style={{ display: "inline-block" }}
              >
                <Dropdown.Item onBlur={(e) => this.setFilter(1, "Population")}>
                  Population
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(2, "AQI")}>
                  AQI
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(3, "CO2/Capita")}>
                  CO2/Capita
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(4, "Greenery")}>
                  % Greenery
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(5, "GGE")}>
                  GGE
                </Dropdown.Item>
              </DropdownButton>
              <span style={{ margin: "5px" }}>from</span>
              <input
                type="text"
                name="start"
                onBlur={(e) => this.setStart(e.target.value)}
              />
              <span style={{ margin: "5px" }}>to</span>
              <input
                type="text"
                name="end"
                onBlur={(e) => this.setEnd(e.target.value)}
              />
            </label>
            <label style={{ margin: "20px" }}>
              <DropdownButton
                variant="light"
                id="dropdown-basic-button"
                title={this.state.sort}
                style={{ display: "inline-block" }}
              >
                <Dropdown.Item onBlur={(e) => this.setSort(1, "Population")}>
                  Population
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(2, "AQI")}>
                  AQI
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(3, "CO2/Capita")}>
                  CO2/Capita
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(4, "Greenery")}>
                  % Greenery
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(5, "GGE")}>
                  GGE
                </Dropdown.Item>
              </DropdownButton>
              <Button
                variant="light"
                onClick={(e) => this.setAsc()}
                style={{ marginLeft: "5px" }}
              >
                {" "}
                ASC{" "}
              </Button>

              <Button
                variant="light"
                onClick={(e) => this.setDesc()}
                style={{ margin: "2px" }}
              >
                {" "}
                DESC{" "}
              </Button>

              <Button
                variant="dark"
                onClick={(e) => this.updateCities()}
                style={{ margin: "20px" }}
              >
                {" "}
                Go{" "}
              </Button>
            </label>
          </div>
          <div className="container-fluid text-left">
            <CardDeck>{this.makeCards()}</CardDeck>
          </div>
          <div>{this.buildNav()}</div>
        </div>
      </div>
    );
  }
}

export default Cities;
