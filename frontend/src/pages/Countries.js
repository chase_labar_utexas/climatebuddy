import React, { Component } from "react";
import {
  Card,
  CardDeck,
  Nav,
  Table,
  Col,
  Button,
  Dropdown,
  DropdownButton,
} from "react-bootstrap";
import $ from "jquery";
import NavBar from "../components/Navbar.js";
import Popper from "popper.js";
import "../css/countries.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap/dist/css/bootstrap.min.css";

class Countries extends Component {
  constructor() {
    super();
    this.state = {
      countries: [],
      pageno: 0,
      pagelen: 20,
      sort: "Sort By",
      filter: "Filter By",
      curCountries: [],
    };
    this.search = "";
    this.filterBy = -1;

    this.start = "";
    this.end = "";
    this.sortBy = -1;

    this.order = 0;
  }
  componentDidMount() {
    fetch(`https://api.climatebuddy.xyz/countries`)
      .then((response) => response.json())
      .then((response) => {
        this.setState({ countries: response, curCountries: response });
      });
  }

  updateCountries() {
    this.state.curCountries = [];

    // SEARCHING
    var len = this.search.length;
    if (len != 0) {
      var words = this.search.split(" ");
      for (let i = 0; i < words.length; i++) {
        len = words[i].length;
        if (len != 0) {
          var keyword = words[i].toLowerCase();
          for (let j = 0; j < this.state.countries.length; j++) {
            var instance = this.state.countries[j];
            var percent = (instance.HDI * 100).toFixed(2);
            if (
              String(instance.name).toLowerCase().includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
            else if (
              String(instance.population).includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
            else if (
              String(instance.GDP).includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
            else if (
              String(instance.avgLifespan).includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
            else if (
              String(percent).includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
            else if (
              String(instance.continent).includes(keyword) &&
              !this.state.curCountries.includes(instance)
            )
              this.state.curCountries.push(instance);
          }
        }
      }
    } else {
      this.state.curCountries = this.state.countries.slice();
    }

    // FILTERING
    var tempcountries = [];
    if (this.start != "" && this.end != "") {
      for (let i = 0; i < this.state.curCountries.length; i++) {
        var instance = this.state.curCountries[i];
        switch (this.filterBy) {
          case 1:
            if (
              instance.population >= this.start &&
              instance.population < this.end
            )
              tempcountries.push(instance);
            break;
          case 2:
            if (instance.GDP >= this.start && instance.GDP < this.end)
              tempcountries.push(instance);
            break;
          case 3:
            if (
              instance.avgLifespan >= this.start &&
              instance.avgLifespan < this.end
            )
              tempcountries.push(instance);
            break;
          case 4:
            var percent = (instance.HDI * 100).toFixed(2);
            if (Number(percent) >= this.start && Number(percent) < this.end)
              tempcountries.push(instance);
            break;
          case 5:
            if (
              instance.continent >= this.start &&
              instance.continent < this.end
            )
              tempcountries.push(instance);
            break;
          default:
            tempcountries.push(instance);
            break;
        }
      }
      this.state.curCountries = tempcountries;
    }

    // SORTING
    if (this.sortBy != -1) {
      //console.log(this.state.curCountries.length)
      this.state.curCountries = this.quickSortAsc(
        this.state.curCountries,
        this.order
      );
      //console.log(this.order)
      //console.log(this.state.curCountries.length)
    }

    this.setState({
      curCountries: this.state.curCountries,
      pageno: 0,
    });

    this.makeCards();
  }

  quickSortAsc(array, order) {
    if (array.length <= 1) {
      return array;
    } else {
      var multiplier = 1;
      if (order == 1) {
        multiplier = -1;
      }

      var left = [];
      var right = [];
      var newArray = [];
      var pivot = array.pop();
      var length = array.length;

      for (var i = 0; i < length; i++) {
        var instance = array[i];
        switch (this.sortBy) {
          case 1:
            //console.log(pivot)
            if (
              multiplier * instance.population <=
              multiplier * pivot.population
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 2:
            if (multiplier * instance.GDP <= multiplier * pivot.GDP) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 3:
            if (
              multiplier * instance.avgLifespan <=
              multiplier * pivot.avgLifespan
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 4:
            if (multiplier * instance.HDI <= multiplier * pivot.HDI) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
          case 5:
            if (
              multiplier * instance.continent <=
              multiplier * pivot.continent
            ) {
              left.push(array[i]);
            } else {
              right.push(array[i]);
            }
            break;
        }
      }
      return newArray.concat(
        this.quickSortAsc(left, order),
        pivot,
        this.quickSortAsc(right, order)
      );
    }
  }

  makeCards() {
    var styles = { width: "18vw", flexBasis: "auto" };
    var pageof = this.state.pagelen * this.state.pageno;
    let rows = [];
    for (let r = 0; r < 4; r++) {
      let cols = [];
      for (let c = 0; c < 5; c++) {
        if (this.state.curCountries.length <= pageof + c + r * 5) {
          break;
        }
        var instance = this.state.curCountries[pageof + c + r * 5];
        var GDP = instance.GDP;
        var LS = instance.avgLifespan;
        var HDI = instance.HDI;
        if (GDP == undefined) GDP = "N/A";
        else {
          GDP = instance.GDP.toLocaleString(navigator.language, {
            minimumFractionDigits: 2,
          });
        }
        if (LS == null) LS = "N/A";
        else
          LS = instance.avgLifespan.toLocaleString(navigator.language, {
            minimumFractionDigits: 2,
          });
        if (HDI == null) HDI = "N/A";
        else
          HDI = instance.HDI.toLocaleString(navigator.language, {
            minimumFractionDigits: 2,
          });

        cols.push(
          <Col style={styles}>
            <Card className="instance-box text-left" style={{ width: "17vw" }}>
              <img
                src={
                  "https://www.countryflags.io/" +
                  String(instance.iso2) +
                  "/shiny/64.png"
                }
                style={{ margin: ".5em 1em 0" }}
              ></img>
              <Nav.Link
                className="instance-header"
                href={"/country/" + +String(instance.countryID)}
              >
                {" "}
                {instance.name}{" "}
              </Nav.Link>
              <Card.Body>
                <Card.Text> Population: {instance.population} </Card.Text>
                <Card.Text> GDP: {GDP} </Card.Text>
                <Card.Text> Average Lifespan: {LS} </Card.Text>
                <Card.Text> HDI: {HDI} </Card.Text>
                <Card.Text> Continent: {instance.continent} </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        );
      }
      rows.push(<br></br>);
      rows.push(<div className="row instance-padding m-auto">{cols}</div>);
    }
    return rows;
  }

  prevPage() {
    if (this.state.pageno > 0) {
      this.setState({ pageno: this.state.pageno - 1 });
      this.makeCards();
    }
  }

  nextPage() {
    var totpage = Math.floor(
      this.state.curCountries.length / this.state.pagelen
    );
    if (this.state.pageno < totpage) {
      this.setState({ pageno: this.state.pageno + 1 });
      this.makeCards();
    }
  }

  getPage(i) {
    var totpage = Math.ceil(
      this.state.curCountries.length / this.state.pagelen
    );
    if (i >= 0 && i < totpage) {
      this.setState({ pageno: i });
      this.makeCards();
    }
  }

  buildNav() {
    var totpage = Math.floor(
      this.state.curCountries.length / this.state.pagelen
    );
    var curPage = this.state.pageno;
    var cols = [];
    cols.push(
      <Button variant="light" onClick={(e) => this.getPage(0)}>
        &lt;&lt;
      </Button>
    );
    cols.push(
      <Button variant="light" onClick={(e) => this.prevPage()}>
        &lt;
      </Button>
    );
    var offset = 2;
    if (curPage - 2 >= 0) {
      cols.push(
        <Button variant="light" onClick={(e) => this.getPage(curPage - 2)}>
          {curPage - 1}
        </Button>
      );
    } else offset++;
    if (curPage - 1 >= 0) {
      cols.push(
        <Button variant="light" onClick={(e) => this.getPage(curPage - 1)}>
          {curPage}
        </Button>
      );
    } else offset++;
    if (curPage >= 0) {
      cols.push(
        <Button variant="dark" onClick={(e) => this.getPage(curPage)}>
          {curPage + 1}
        </Button>
      );
    } else console.log("ERROR");

    for (var i = 1; i <= offset; i++) {
      cols.push(this.createCol(curPage + i));
    }

    cols.push(
      <Button variant="light" onClick={(e) => this.nextPage()}>
        &gt;
      </Button>
    );

    cols.push(
      <Button
        variant="light"
        onClick={(e) =>
          this.getPage(
            Math.ceil(this.state.curCountries.length / this.state.pagelen) - 1
          )
        }
      >
        &gt;&gt;
      </Button>
    );
    console.log(cols + "hi");
    return cols;
  }

  createCol(i) {
    return (
      <Button variant="light" value={i} onClick={(e) => this.getPage(i)}>
        {i + 1}
      </Button>
    );
  }

  render() {
    return (
      <div className="App">
        <NavBar />
        <div className="container-fluid text-center">
          <div className>
            <p className="head-text m-0">
              Countries: {this.state.curCountries.length}
            </p>
          </div>
          <p className="head-bottom-text">
            Find your country and see how it stacks up!
          </p>
          <div>{this.buildNav()}</div>
          <div>
            <label style={{ margin: "20px" }}>
              {" "}
              Search:{" "}
              <input
                type="text"
                name="search"
                onBlur={(e) => this.setSearch(e.target.value)}
              />
            </label>
            <label>
              <DropdownButton
                variant="light"
                id="dropdown-basic-button"
                title={this.state.filter}
                style={{ display: "inline-block" }}
              >
                <Dropdown.Item onBlur={(e) => this.setFilter(1, "Population")}>
                  Population
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(2, "GDP")}>
                  GDP
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(3, "Lifespan")}>
                  Lifespan
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(4, "HDI")}>
                  HDI
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setFilter(5, "Continent")}>
                  Continent
                </Dropdown.Item>
              </DropdownButton>
              <span style={{ margin: "5px" }}>from</span>
              <input
                type="text"
                name="start"
                onBlur={(e) => this.setStart(e.target.value)}
              />
              <span style={{ margin: "5px" }}>to</span>
              <input
                type="text"
                name="end"
                onBlur={(e) => this.setEnd(e.target.value)}
              />
            </label>
            <label style={{ margin: "20px" }}>
              <DropdownButton
                variant="light"
                id="dropdown-basic-button"
                title={this.state.sort}
                style={{ display: "inline-block" }}
              >
                <Dropdown.Item onBlur={(e) => this.setSort(1, "Population")}>
                  Population
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(2, "GDP")}>
                  GDP
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(3, "Lifespan")}>
                  Lifespan
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(4, "HDI")}>
                  HDI
                </Dropdown.Item>
                <Dropdown.Item onBlur={(e) => this.setSort(5, "Continent")}>
                  Continent
                </Dropdown.Item>
              </DropdownButton>
              <Button
                variant="light"
                onClick={(e) => this.setAsc()}
                style={{ marginLeft: "5px" }}
              >
                {" "}
                ASC{" "}
              </Button>

              <Button
                variant="light"
                onClick={(e) => this.setDesc()}
                style={{ margin: "2px" }}
              >
                {" "}
                DESC{" "}
              </Button>

              <Button
                variant="dark"
                onClick={(e) => this.updateCountries()}
                style={{ margin: "20px" }}
              >
                {" "}
                Go{" "}
              </Button>
            </label>
          </div>
          <div className="container-fluid text-center">
            <CardDeck>{this.makeCards()}</CardDeck>
          </div>
          <div style={{ marginTop: "20px", marginBottom: "30px" }}>
            {this.buildNav()}
          </div>
        </div>
      </div>
    );
  }

  setAsc() {
    this.order = 0;
  }

  setDesc() {
    this.order = 1;
  }

  setFilter(value, name) {
    //console.log(name + "hi")
    this.filterBy = value;
    this.setState({
      filter: name,
    });
  }

  setSort(value, name) {
    this.sortBy = value;
    console.log(name);
    this.setState({
      sort: name,
    });
  }

  setSearch(value) {
    console.log(value + "valueasdf asdf afsd fasd fasd asdf ");
    this.search = value;
  }

  setStart(value) {
    this.start = value;
  }

  setEnd(value) {
    this.end = value;
  }
}

export default Countries;
