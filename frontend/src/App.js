import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//import NavBar from './components/Navbar.js';
import Cities from './pages/Cities';
import Countries from './pages/Countries';
import Homepage from './pages/Homepage';
import Worldview from './pages/Worldview';
import About from './pages/About';
import Visualizations from './pages/Visualizations';
import CityInstance from './pages/CityInstance';
import CountryInstance from './pages/CountryInstance';
import WorldviewInstance from './pages/WorldviewInstance';
import Search from './pages/Search';
import './css/parent.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className = "App">
          <Switch>
            <Route exact path={'/'} component={Homepage} />
            <Route exact path={'/search'} component={Search} />
            <Route exact path={'/about'} component={About} />
            <Route exact path={'/cities'} component={Cities} />
            <Route exact path={'/countries'} component={Countries} />
            <Route exact path={'/homepage'} component={Homepage} />
            <Route exact path={'/worldviews'} component={Worldview} />
            <Route exact path={'/visualizations'} component={Visualizations} />
            <Route exact path = {'/city/:cityID'} component={CityInstance} />
            <Route exact path = {'/country/:countryID'} component={CountryInstance} />
            <Route exact path = {'/worldview/:statID'} component={WorldviewInstance} />

          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
