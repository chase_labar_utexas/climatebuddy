
import React from 'react';
import ReactDOM from 'react-dom';
import { expect, use } from 'chai';

import Cities from '../pages/Cities';
import Countries from '../pages/Countries';
import Homepage from '../pages/Homepage';
import Worldview from '../pages/Worldview';
import About from '../pages/About';
import CityInstance from '../pages/CityInstance'
import CountryInstance from '../pages/CountryInstance'
import WorldviewInstance from '../pages/WorldviewInstance'

var assert = require('assert')

it('should get Cities', async () => {
	const copy = shallow(<Cities />);
	expect(copy).to.not.be.undefined;
	expect(copy).to.have.length(1);
	expect(copy.find("div")).to.have.length(7);
});

it('should get Countries', async () => {
	const copy = shallow(<Countries />);
	expect(copy).to.not.be.undefined;
	expect(copy).to.have.length(1);
	expect(copy.find("div")).to.have.length(7);
});

it('should get Homepage', async () => {
	const copy = shallow(<Homepage />);
	expect(copy).to.not.be.undefined;
	expect(copy).to.have.length(1);
	expect(copy.find("div")).to.have.length(12);
});

it('should get Worldview', async () => {
	const copy = shallow(<Worldview />);
	expect(copy).to.not.be.undefined;
	expect(copy).to.have.length(1);
	expect(copy.find("div")).to.have.length(3);
});

it('should get About', async () => {
	const copy = shallow(<About />);
	expect(copy).to.not.be.undefined;
	expect(copy).to.have.length(1);
	expect(copy.find("div")).to.have.length(42);
});

it('should get CityInstance', async () => {
	const component = mount(<CityInstance />);
	expect(component).to.not.be.undefined;
	expect(component).to.have.length(1);
});

it('should get CountryInstance', async () => {
	const component = mount(<CountryInstance />);
	expect(component).to.not.be.undefined;
	expect(component).to.have.length(1);
});

it('should get WorldviewInstance', async () => {
	const component = mount(<WorldviewInstance />);
	expect(component).to.not.be.undefined;
	expect(component).to.have.length(1);
});

