import unittest
import os

from selenium import webdriver


class selenium_tests(unittest.TestCase): 
	def setUp(self): 
		exec_path = os.path.join(os.getcwd(), "geckodriver.exe")
		if os.path.exists(exec_path): 
			self.driver = webdriver.Firefox(executable_path=exec_path)
		elif: 
			self.driver = webdriver.Firefox()

	def test_worldview(self): 
		self.driver.get("https://www.climatebuddy.xyz")
		worldview_button = self.driver.find_element_by_xpath("//*[@id="navbarNav"]/ul/li[2]/a")
		worldview_button.click()
		assert "worldview" in self.driver.current_url
		cards = self.driver.find_element_by_css_selector("instance-box")
		assert len(cards) == 10
		for card in cards: 
			labels = card.find_element_by_tag_name("Card.Text")
			assert len(labels) == 4
			assert "Highest Value Country" in labels[0].text
			assert "Lowest Value Country" in labels[1].text
			assert "Mean of Statistics" in labels[2].text
			assert "Number of Samples" in labels[3].text

	def test_countries(self): 
		self.driver.get("https://www.climatebuddy.xyz")
		worldview_button = self.driver.find_element_by_xpath("//*[@id="navbarNav"]/ul/li[3]/a")
		worldview_button.click()
		assert "countries" in self.driver.current_url
		cards = self.driver.find_element_by_css_selector("instance-box text-left")
		assert len(cards) == 20
		for card in cards: 
			labels = card.find_element_by_tag_name("Card.Text")
			assert len(labels) == 4
			assert "Population" in labels[0].text
			assert "GDP" in labels[1].text
			assert "Average Lifespan" in labels[2].text
			assert "Continent" in labels[3].text

	def test_citites(self): 
		self.driver.get("https://www.climatebuddy.xyz")
		worldview_button = self.driver.find_element_by_xpath("//*[@id="navbarNav"]/ul/li[4]/a")
		worldview_button.click()
		assert "cities" in self.driver.current_url
		cards = self.driver.find_element_by_css_selector("instance-box")
		assert len(cards) == 20
		for card in cards: 
			labels = card.find_element_by_tag_name("Card.Text")
			assert len(labels) == 5
			assert "Population" in labels[0].text
			assert "Air Quality" in labels[1].text
			assert "CO2/Capita" in labels[2].text
			assert "Percent Greenery" in labels[3].text
			assert "Greenhouse Gas Emission" in labels[4].text

	def test_about(self): 
		self.driver.get("https://www.climatebuddy.xyz")
		about_button = self.driver.find_element_by_xpath("//*[@id="navbarNav"]/ul/li[5]/a")
		about_button.click()
		assert "about" in self.driver.current_url
		headers = self.driver.find_element_by_css_selector("head-text m-0")
		assert len(headers) == 4
		team = self.driver.find_element_by_css_selector("instance-box")
		assert len(team) == 5
		for member in team: 
			bio_info = member.find_element_by_tag_name("p")
			prof_pic = member.find_element_by_tag_name("img")
			assert len(bio_info) == 4
			assert len(prof_pic) == 1

	def tearDown(self): 
		self.driver.quit()


if __name__ == "__main__": 
	unittest.main()
